<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Externals Definitions</title><link rel="stylesheet" href="styles.css" type="text/css" /><meta name="generator" content="DocBook XSL Stylesheets V1.71.0" /><link rel="start" href="index.html" title="Управление версиями в Subversion" /><link rel="up" href="svn.advanced.html" title="Глава 7. Профессиональное использование Subversion" /><link rel="prev" href="svn.advanced.pegrevs.html" title="Peg and Operative Revisions" /><link rel="next" href="svn.advanced.vendorbr.html" title="Vendor branches" /></head><body><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Externals Definitions</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="svn.advanced.pegrevs.html">Пред.</a> </td><th width="60%" align="center">Глава 7. Профессиональное использование Subversion</th><td width="20%" align="right"> <a accesskey="n" href="svn.advanced.vendorbr.html">След.</a></td></tr></table><hr /></div><div class="sect1" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a id="svn.advanced.externals"></a>Externals Definitions</h2></div></div></div><p>Sometimes it is useful to construct a working copy that is
      made out of a number of different checkouts.  For example, you
      may want different subdirectories to come from different
      locations in a repository, or perhaps from different
      repositories altogether.  You could certainly setup such a
      scenario by hand—using <span><strong class="command">svn checkout</strong></span> to
      create the sort of nested working copy structure you are trying
      to achieve.  But if this layout is important for everyone who
      uses your repository, every other user will need to perform the
      same checkout operations that you did.</p><p>Fortunately, Subversion provides support for
      <em class="firstterm">externals definitions</em>.  An externals
      definition is a mapping of a local directory to the
      URL—and possibly a particular revision—of a
      versioned resource.  In Subversion, you declare externals
      definitions in groups using the <code class="literal">svn:externals</code>
      property.  You can create or modify this property using
      <span><strong class="command">svn propset</strong></span> or <span><strong class="command">svn
      propedit</strong></span> (see <a href="svn.advanced.props.html#svn.advanced.props.why" title="Зачем нужны свойства?">«Зачем нужны свойства?»</a>).
      It can be set on any versioned directory,
      and its value is a multi-line table of subdirectories (relative
      to the versioned directory on which the property is set) and
      fully qualified, absolute Subversion repository URLs.</p><pre class="screen">
$ svn propget svn:externals calc
third-party/sounds             http://sounds.red-bean.com/repos
third-party/skins              http://skins.red-bean.com/repositories/skinproj
third-party/skins/toolkit -r21 http://svn.red-bean.com/repos/skin-maker
</pre><p>The convenience of the <code class="literal">svn:externals</code>
      property is that once it is set on a versioned directory,
      everyone who checks out a working copy with that directory also
      gets the benefit of the externals definition.  In other words,
      once one person has made the effort to define those nested
      working copy checkouts, no one else has to
      bother—Subversion will, upon checkout of the original
      working copy, also checkout the external working copies.</p><p>Note the previous externals definition example.  When
      someone checks out a working copy of the
      <code class="filename">calc</code> directory, Subversion also continues
      to checkout the items found in its externals definition.</p><pre class="screen">
$ svn checkout http://svn.example.com/repos/calc
A  calc
A  calc/Makefile
A  calc/integer.c
A  calc/button.c
Checked out revision 148.

Fetching external item into calc/third-party/sounds
A  calc/third-party/sounds/ding.ogg
A  calc/third-party/sounds/dong.ogg
A  calc/third-party/sounds/clang.ogg
…
A  calc/third-party/sounds/bang.ogg
A  calc/third-party/sounds/twang.ogg
Checked out revision 14.

Fetching external item into calc/third-party/skins
…
</pre><p>If you need to change the externals definition, you can do
      so using the regular property modification subcommands.  When
      you commit a change to the <code class="literal">svn:externals</code>
      property, Subversion will synchronize the checked-out items
      against the changed externals definition when you next run
      <span><strong class="command">svn update</strong></span>.  The same thing will happen when
      others update their working copies and receive your changes to
      the externals definition.</p><p>The <span><strong class="command">svn status</strong></span> command also recognizes
      externals definitions, displaying a status code of
      <code class="literal">X</code> for the disjoint subdirectories into which
      externals are checked out, and then recursing into those
      subdirectories to display the status of the external items
      themselves.</p><div class="tip" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Подсказка</h3><p>You should strongly consider using explicit revision
        numbers in all of your externals definitions.  Doing so means
        that you get to decide when to pull down a different snapshot
        of external information, and exactly which snapshot to pull.
        Besides the common sense aspect of not being surprised by
        changes to third-party repositories that you might not have
        any control over, using explicit revision numbers also means
        that as you backdate your working copy to a previous
        revision, your externals definitions will also revert to the
        way they looked in that previous revision, which in turn means
        that the external working copies will be updated to match they
        way <span class="emphasis"><em>they</em></span> looked back when your repository was
        at that previous revision.  For software projects, this could
        be the difference between a successful and a failed build of
        an older snapshot of your complex codebase.</p></div><p>The support that exists for externals definitions in
      Subversion today can be a little misleading, though.  First, an
      externals definition can only point to directories, not files.
      Second, the externals definition cannot point to relative paths
      (paths like <code class="filename">../../skins/myskin</code>).  Third, the
      working copies created via the externals definition support are
      still disconnected from the primary working copy (on whose
      versioned directories the <code class="literal">svn:externals</code>
      property was actually set).  And Subversion still only truly
      operates on non-disjoint working copies.  So, for example, if
      you want to commit changes that you've made in one or more of
      those external working copies, you must run <span><strong class="command">svn
      commit</strong></span> explicitly on those working
      copies—committing on the primary working copy will not
      recurse into any external ones.</p><p>Also, since the definitions themselves use absolute URLs,
      moving or copying a directory to which they are attached will
      not affect what gets checked out as an external (though the
      relative local target subdirectory will, of course, move with
      renamed directory).  This can be confusing—even
      frustrating—in certain situations.  For example, if you
      use externals definitions on a directory in your
      <code class="filename">/trunk</code> development line which point to
      other areas of that same line, and then you use <span><strong class="command">svn
      copy</strong></span> to branch that line to some new location
      <code class="filename">/branches/my-branch</code>, the externals
      definitions on items in your new branch will still refer to
      versioned resources in <code class="filename">/trunk</code>.  Be aware,
      too, that if you need to re-parent your working copy (using
      <span><strong class="command">svn switch --relocate</strong></span>), externals definitions
      will <span class="emphasis"><em>not</em></span> also be re-parented.</p><p>Finally, there might be times when you would prefer that
      <span><strong class="command">svn</strong></span> subcommands would not recognize or
      otherwise operate on the external working copies created as the
      result of externals definition handling.  In those instances,
      you can pass the <code class="option">--ignore-externals</code> option to
      the subcommand.</p></div><div class="navfooter"><hr /><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="svn.advanced.pegrevs.html">Пред.</a> </td><td width="20%" align="center"><a accesskey="u" href="svn.advanced.html">Уровень выше</a></td><td width="40%" align="right"> <a accesskey="n" href="svn.advanced.vendorbr.html">След.</a></td></tr><tr><td width="40%" align="left" valign="top">Peg and Operative Revisions </td><td width="20%" align="center"><a accesskey="h" href="index.html">Начало</a></td><td width="40%" align="right" valign="top"> Vendor branches</td></tr></table></div></body></html>
