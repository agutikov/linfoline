<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>svnserve, a custom server</title><link rel="stylesheet" href="styles.css" type="text/css" /><meta name="generator" content="DocBook XSL Stylesheets V1.71.0" /><link rel="start" href="index.html" title="Управление версиями в Subversion" /><link rel="up" href="svn.serverconfig.html" title="Глава 6. Настройка сервера" /><link rel="prev" href="svn.serverconfig.netmodel.html" title="Сетевая модель" /><link rel="next" href="svn.serverconfig.httpd.html" title="httpd, the Apache HTTP server" /></head><body><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">svnserve, a custom server</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="svn.serverconfig.netmodel.html">Пред.</a> </td><th width="60%" align="center">Глава 6. Настройка сервера</th><td width="20%" align="right"> <a accesskey="n" href="svn.serverconfig.httpd.html">След.</a></td></tr></table><hr /></div><div class="sect1" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a id="svn.serverconfig.svnserve"></a>svnserve, a custom server</h2></div></div></div><p>The <span><strong class="command">svnserve</strong></span> program is a lightweight
      server, capable of speaking to clients over TCP/IP using a
      custom, stateful protocol.  Clients contact an
      <span><strong class="command">svnserve</strong></span> server by using URLs that begin with
      the <code class="literal">svn://</code> or <code class="literal">svn+ssh://</code>
      schema.  This section will explain the different ways of running
      <span><strong class="command">svnserve</strong></span>, how clients authenticate themselves
      to the server, and how to configure appropriate access control
      to your repositories.</p><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.serverconfig.svnserve.invoking"></a>Invoking the Server</h3></div></div></div><p>There are a few different ways to invoke the
        <span><strong class="command">svnserve</strong></span> program.  If invoked with no
        options, you'll see nothing but a help message.  However, if
        you're planning to have <span><strong class="command">inetd</strong></span> launch the
        process, then you can pass the <code class="option">-i</code>
        (<code class="option">--inetd</code>) option:</p><pre class="screen">
$ svnserve -i
( success ( 1 2 ( ANONYMOUS ) ( edit-pipeline ) ) )
</pre><p>When invoked with the <code class="option">--inetd</code> option,
        <span><strong class="command">svnserve</strong></span> attempts to speak with a
        Subversion client via <span class="emphasis"><em>stdin</em></span> and
        <span class="emphasis"><em>stdout</em></span> using a custom protocol.  This is
        the standard behavior for a program being run via
        <span><strong class="command">inetd</strong></span>.  The IANA has reserved port 3690
        for the Subversion protocol, so on a Unix-like system you can
        add lines to <code class="filename">/etc/services</code> like these (if
        they don't already exist):</p><pre class="screen">
svn           3690/tcp   # Subversion
svn           3690/udp   # Subversion
</pre><p>And if your system is using a classic Unix-like
        <span><strong class="command">inetd</strong></span> daemon, you can add this line to
        <code class="filename">/etc/inetd.conf</code>:</p><pre class="screen">
svn stream tcp nowait svnowner /usr/bin/svnserve svnserve -i
</pre><p>Make sure «<span class="quote">svnowner</span>» is a user which has
        appropriate permissions to access your repositories.  Now, when
        a client connection comes into your server on port 3690,
        <span><strong class="command">inetd</strong></span> will spawn an
        <span><strong class="command">svnserve</strong></span> process to service it.</p><p>On a Windows system, third-party tools exist to run
      <span><strong class="command">svnserve</strong></span> as a service.  Look on Subversion's
      website for a list of these tools.</p><p>A second option is to run <span><strong class="command">svnserve</strong></span> as a
        standalone «<span class="quote">daemon</span>» process.  Use the
        <code class="option">-d</code> option for this:</p><pre class="screen">
$ svnserve -d
$               # svnserve is now running, listening on port 3690
</pre><p>When running <span><strong class="command">svnserve</strong></span> in daemon mode,
        you can use the <code class="option">--listen-port=</code> and
        <code class="option">--listen-host=</code> options to customize the exact
        port and hostname to «<span class="quote">bind</span>» to.</p><p>There's still a third way to invoke
        <span><strong class="command">svnserve</strong></span>, and that's in «<span class="quote">tunnel
        mode</span>», with the <code class="option">-t</code> option.  This mode
        assumes that a remote-service program such as
        <span><strong class="command">RSH</strong></span> or <span><strong class="command">SSH</strong></span> has
        successfully authenticated a user and is now invoking a
        private <span><strong class="command">svnserve</strong></span> process <span class="emphasis"><em>as that
        user</em></span>.  The <span><strong class="command">svnserve</strong></span> program
        behaves normally (communicating via <span class="emphasis"><em>stdin</em></span>
        and <span class="emphasis"><em>stdout</em></span>), and assumes that the traffic
        is being automatically redirected over some sort of tunnel
        back to the client.  When <span><strong class="command">svnserve</strong></span> is
        invoked by a tunnel agent like this, be sure that the
        authenticated user has full read and write access to the
        repository database files. (See <a href="svn.serverconfig.svnserve.html#svn.serverconfig.svnserve.invoking.sb-1" title="Servers and Permissions:  A Word of Warning">Servers and Permissions:  A Word of Warning</a>.)  It's essentially the same as
        a local user accessing the repository via
        <code class="literal">file:///</code> URLs.</p><div class="sidebar"><a id="svn.serverconfig.svnserve.invoking.sb-1"></a><p class="title"><b>Servers and Permissions:  A Word of Warning</b></p><p>First, remember that a Subversion repository is a
          collection of database files; any process which accesses the
          repository directly needs to have proper read and write
          permissions on the entire repository.  If you're not
          careful, this can lead to a number of headaches, especially
          if you're using a Berkeley DB database rather than FSFS.  Be
          sure to read <a href="svn.serverconfig.multimethod.html" title="Supporting Multiple Repository Access Methods">«Supporting Multiple Repository Access Methods»</a>.</p><p>Secondly, when configuring <span><strong class="command">svnserve</strong></span>,
          Apache <span><strong class="command">httpd</strong></span>, or any other server
          process, keep in mind that you might not want to launch the
          server process as the user <code class="literal">root</code> (or as
          any other user with unlimited permissions).  Depending on
          the ownership and permissions of the repositories you're
          exporting, it's often prudent to use a
          different—perhaps custom—user.  For example,
          many administrators create a new user named
          <code class="literal">svn</code>, grant that user exclusive ownership
          and rights to the exported Subversion repositories, and only
          run their server processes as that user.</p></div><p>Once the <span><strong class="command">svnserve</strong></span> program is running,
        it makes every repository on your system available to the
        network.  A client needs to specify an
        <span class="emphasis"><em>absolute</em></span> path in the repository URL.  For
        example, if a repository is located at
        <code class="filename">/usr/local/repositories/project1</code>, then a
        client would reach it via <code class="systemitem">svn://host.example.com/usr/local/repositories/project1
        </code>.  To increase security, you can pass the
        <code class="option">-r</code> option to <span><strong class="command">svnserve</strong></span>,
        which restricts it to exporting only repositories below that
        path:</p><pre class="screen">
$ svnserve -d -r /usr/local/repositories
…
</pre><p>Using the <code class="option">-r</code> option effectively
        modifies the location that the program treats as the root of
        the remote filesystem space.  Clients then use URLs that
        have that path portion removed from them, leaving much
        shorter (and much less revealing) URLs:</p><pre class="screen">
$ svn checkout svn://host.example.com/project1
…
</pre></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.serverconfig.svnserve.auth"></a>Built-in authentication and authorization</h3></div></div></div><p>When a client connects to an <span><strong class="command">svnserve</strong></span>
        process, the following things happen:</p><div class="itemizedlist"><ul type="disc"><li><p>The client selects a specific
        repository.</p></li><li><p>The server processes the repository's
        <code class="filename">conf/svnserve.conf</code> file, and begins to
        enforce any authentication and authorization policies defined
        therein.</p></li><li><p>Depending on the situation and authorization
        policies,</p><div class="itemizedlist"><ul type="circle"><li><p>the client may be allowed to make requests
              anonymously, without ever receiving an authentication
              challenge, OR</p></li><li><p>the client may be challenged for
              authentication at any time, OR</p></li><li><p>if operating in «<span class="quote">tunnel
              mode</span>», the client will declare itself to be
              already externally authenticated.</p></li></ul></div></li></ul></div><p>At the time of writing, the server only knows how to send
        a CRAM-MD5 <sup>[<a id="id350701" href="#ftn.id350701">29</a>]</sup>
        authentication challenge.  In essence, the server sends a bit
        of data to the client.  The client uses the MD5 hash algorithm
        to create a fingerprint of the data and password combined,
        then sends the fingerprint as a response.  The server performs
        the same computation with the stored password to verify that
        the result is identical.  <span class="emphasis"><em>At no point does the
        actual password travel over the network.</em></span></p><p>It's also possible, of course, for the client to be
        externally authenticated via a tunnel agent, such as
        <span><strong class="command">SSH</strong></span>.  In that case, the server simply
        examines the user it's running as, and uses it as the
        authenticated username.  For more on this, see <a href="svn.serverconfig.svnserve.html#svn.serverconfig.svnserve.sshauth" title="SSH authentication and authorization">«SSH authentication and authorization»</a>.</p><p>As you've already guessed, a repository's
        <code class="filename">svnserve.conf</code> file is the central
        mechanism for controlling authentication and authorization
        policies.  The file has the same format as other configuration
        files (see <a href="svn.advanced.html#svn.advanced.confarea" title="Параметры времени выполнения">«Параметры времени выполнения»</a>): section names
        are marked by square brackets (<code class="literal">[</code> and
        <code class="literal">]</code>), comments begin with hashes
        (<code class="literal">#</code>), and each section contains
        specific variables that can be set (<code class="literal">variable =
        value</code>).  Let's walk through this file and learn how
        to use them.</p><div class="sect3" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h4 class="title"><a id="svn.serverconfig.svnserve.auth.users"></a>Create a 'users' file and realm</h4></div></div></div><p>For now, the <code class="literal">[general]</code> section of the
          <code class="filename">svnserve.conf</code> has all the variables you
          need.  Begin by defining a file which contains usernames and
          passwords, and an authentication realm:</p><pre class="screen">
[general]
password-db = userfile
realm = example realm
</pre><p>The <code class="literal">realm</code> is a name that you define.
          It tells clients which sort of «<span class="quote">authentication
          namespace</span>» they're connecting to; the Subversion
          client displays it in the authentication prompt, and uses it
          as a key (along with the server's hostname and port) for
          caching credentials on disk (see <a href="svn.serverconfig.netmodel.html#svn.serverconfig.netmodel.credcache" title="Кеширование клиентской идентификационной информации">«Кеширование клиентской идентификационной информации»</a>).  The
          <code class="literal">password-db</code> variable points to a separate
          file that contains a list of usernames and passwords, using
          the same familiar format.  For example:</p><pre class="screen">
[users]
harry = foopassword
sally = barpassword
</pre><p>The value of <code class="literal">password-db</code> can be an
          absolute or relative path to the users file.  For many
          admins, it's easy to keep the file right in the
          <code class="filename">conf/</code> area of the repository, alongside
          <code class="filename">svnserve.conf</code>.  On the other hand, it's
          possible you may want to have two or more repositories share
          the same users file; in that case, the file should probably
          live in a more public place.  The repositories sharing the
          users file should also be configured to have the same realm,
          since the list of users essentially defines an
          authentication realm.  Wherever the file lives, be sure to
          set the file's read and write permissions appropriately.  If
          you know which user(s) <span><strong class="command">svnserve</strong></span> will run
          as, restrict read access to the user file as necessary.</p></div><div class="sect3" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h4 class="title"><a id="svn.serverconfig.svnserve.auth.general"></a>Set access controls</h4></div></div></div><p>There are two more variables to set in the
          <code class="filename">svnserve.conf</code> file: they determine what
          unauthenticated (anonymous) and authenticated users are
          allowed to do.  The variables <code class="literal">anon-access</code>
          and <code class="literal">auth-access</code> can be set to the values
          <code class="literal">none</code>, <code class="literal">read</code>, or
          <code class="literal">write</code>.  Setting the value to
          <code class="literal">none</code> restricts all access of any kind;
          <code class="literal">read</code> allows read-only access to the
          repository, and <code class="literal">write</code> allows complete
          read/write access to the repository.  For example:</p><pre class="screen">
[general]
password-db = userfile
realm = example realm

# anonymous users can only read the repository
anon-access = read

# authenticated users can both read and write
auth-access = write
</pre><p>The example settings are, in fact, the default values of
          the variables, should you forget to define them.  If you
          want to be even more conservative, you can block anonymous
          access completely:</p><pre class="screen">
[general]
password-db = userfile
realm = example realm

# anonymous users aren't allowed
anon-access = none

# authenticated users can both read and write
auth-access = write
</pre><p>Notice that <span><strong class="command">svnserve</strong></span> only understands
          «<span class="quote">blanket</span>» access control.  A user either has
          universal read/write access, universal read access, or no
          access.  There is no detailed control over access to
          specific paths within the repository.  For many projects and
          sites, this level of access control is more than adequate.
          However, if you need per-directory access control, you'll
          need to use either use Apache with
          <span><strong class="command">mod_authz_svn</strong></span> (see <a href="svn.serverconfig.httpd.html#svn.serverconfig.httpd.authz.perdir" title="Per-Directory Access Control">«Per-Directory Access Control»</a>) or use a
          <span><strong class="command">pre-commit</strong></span> hook script to control write
          access (see <a href="svn.reposadmin.create.html#svn.reposadmin.create.hooks" title="Hook Scripts">«Hook Scripts»</a>).  The
          Subversion distribution comes with
          <span><strong class="command">commit-access-control.pl</strong></span> and the more
          sophisticated <span><strong class="command">svnperms.py</strong></span> scripts for use
          in pre-commit scripts.</p></div></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.serverconfig.svnserve.sshauth"></a>SSH authentication and authorization</h3></div></div></div><p><span><strong class="command">svnserve</strong></span>'s built-in authentication can
        be very handy, because it avoids the need to create real
        system accounts.  On the other hand, some administrators
        already have well-established SSH authentication frameworks in
        place.  In these situations, all of the project's users
        already have system accounts and the ability to «<span class="quote">SSH
        into</span>» the server machine.</p><p>It's easy to use SSH in conjunction with
        <span><strong class="command">svnserve</strong></span>.  The client simply uses the
        <code class="literal">svn+ssh://</code> URL schema to connect:</p><pre class="screen">
$ whoami
harry

$ svn list svn+ssh://host.example.com/repos/project
harry@host.example.com's password:  *****

foo
bar
baz
…
</pre><p>In this example, the Subversion client is invoking a local
        <span><strong class="command">ssh</strong></span> process, connecting to
        <code class="literal">host.example.com</code>, authenticating as the
        user <code class="literal">harry</code>, then spawning a private
        <span><strong class="command">svnserve</strong></span> process on the remote machine
        running as the user <code class="literal">harry</code>.  The
        <span><strong class="command">svnserve</strong></span> command is being invoked in tunnel
        mode (<code class="option">-t</code>) and its network protocol is being
        «<span class="quote">tunneled</span>» over the encrypted connection by
        <span><strong class="command">ssh</strong></span>, the tunnel-agent.
        <span><strong class="command">svnserve</strong></span> is aware that it's running as the
        user <code class="literal">harry</code>, and if the client performs a
        commit, the authenticated username will be attributed as the
        author of the new revision.</p><p>The important thing to understand here is that the
        Subversion client is <span class="emphasis"><em>not</em></span> connecting to a
        running <span><strong class="command">svnserve</strong></span> daemon.  This method of
        access doesn't require a daemon, nor does it notice one if
        present.  It relies wholly on the ability of
        <span><strong class="command">ssh</strong></span> to spawn a temporary
        <span><strong class="command">svnserve</strong></span> process, which then terminates
        when the network connection is closed.</p><p>When using <code class="literal">svn+ssh://</code> URLs to access a
        repository, remember that it's the <span><strong class="command">ssh</strong></span>
        program prompting for authentication, and
        <span class="emphasis"><em>not</em></span> the <span><strong class="command">svn</strong></span> client
        program.  That means there's no automatic password caching
        going on (see <a href="svn.serverconfig.netmodel.html#svn.serverconfig.netmodel.credcache" title="Кеширование клиентской идентификационной информации">«Кеширование клиентской идентификационной информации»</a>).  The
        Subversion client often makes multiple connections to the
        repository, though users don't normally notice this due to the
        password caching feature.  When using
        <code class="literal">svn+ssh://</code> URLs, however, users may be
        annoyed by <span><strong class="command">ssh</strong></span> repeatedly asking for a
        password for every outbound connection.  The solution is to
        use a separate SSH password-caching tool like
        <span><strong class="command">ssh-agent</strong></span> on a Unix-like system, or
        <span><strong class="command">pageant</strong></span> on Windows.</p><p>When running over a tunnel, authorization is primarily
        controlled by operating system permissions to the repository's
        database files; it's very much the same as if Harry were
        accessing the repository directly via a
        <code class="literal">file:///</code> URL.  If multiple system users are
        going to be accessing the repository directly, you may want to
        place them into a common group, and you'll need to be careful
        about umasks.  (Be sure to read <a href="svn.serverconfig.multimethod.html" title="Supporting Multiple Repository Access Methods">«Supporting Multiple Repository Access Methods»</a>.)  But even in the case of
        tunneling, the <code class="filename">svnserve.conf</code> file can
        still be used to block access, by simply setting
        <code class="literal">auth-access = read</code> or <code class="literal">auth-access
        = none</code>.</p><p>You'd think that the story of SSH tunneling would end
        here, but it doesn't.  Subversion allows you to create custom
        tunnel behaviors in your run-time <code class="filename">config</code>
        file (see <a href="svn.advanced.html#svn.advanced.confarea" title="Параметры времени выполнения">«Параметры времени выполнения»</a>).  For example,
        suppose you want to use RSH instead of SSH.  In the
        <code class="literal">[tunnels]</code> section of your
        <code class="filename">config</code> file, simply define it like
        this:</p><pre class="screen">
[tunnels]
rsh = rsh
</pre><p>And now, you can use this new tunnel definition by using a
        URL schema that matches the name of your new variable:
        <code class="literal">svn+rsh://host/path</code>.  When using the new
        URL schema, the Subversion client will actually be running the
        command <span><strong class="command">rsh host svnserve -t</strong></span> behind the
        scenes.  If you include a username in the URL (for example,
        <code class="literal">svn+rsh://username@host/path</code>) the client
        will also include that in its command (<span><strong class="command">rsh
        username@host svnserve -t</strong></span>).  But you can define new
        tunneling schemes to be much more clever than that:</p><pre class="screen">
[tunnels]
joessh = $JOESSH /opt/alternate/ssh -p 29934
</pre><p>This example demonstrates a couple of things.  First, it
        shows how to make the Subversion client launch a very specific
        tunneling binary (the one located at
        <code class="filename">/opt/alternate/ssh</code>) with specific
        options.  In this case, accessing a
        <code class="literal">svn+joessh://</code> URL would invoke the
        particular SSH binary with <code class="option">-p 29934</code> as
        arguments—useful if you want the tunnel program to
        connect to a non-standard port.</p><p>Second, it shows how to define a custom environment
        variable that can override the name of the tunneling program.
        Setting the <code class="literal">SVN_SSH</code> environment variable is
        a convenient way to override the default SSH tunnel agent.
        But if you need to have several different overrides for
        different servers, each perhaps contacting a different port or
        passing a different set of options to SSH, you can use the
        mechanism demonstrated in this example.  Now if we were to set
        the <code class="literal">JOESSH</code> environment variable, its value
        would override the entire value of the tunnel
        variable—<span><strong class="command">$JOESSH</strong></span> would be executed
        instead of <span><strong class="command">/opt/alternate/ssh -p
        29934</strong></span>.</p></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.serverconfig.svnserve.sshtricks"></a>SSH configuration tricks</h3></div></div></div><p>It's not only possible to control the way in which the
        client invokes <span><strong class="command">ssh</strong></span>, but also to control
        the behavior of <span><strong class="command">sshd</strong></span> on your server
        machine.  In this section, we'll show how to control the
        exact <span><strong class="command">svnserve</strong></span> command executed
        by <span><strong class="command">sshd</strong></span>, as well as how to have multiple
        users share a single system account.</p><div class="sect3" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h4 class="title"><a id="svn.serverconfig.svnserve.sshtricks.setup"></a>Initial setup</h4></div></div></div><p>To begin, locate the home directory of the account
          you'll be using to launch <span><strong class="command">svnserve</strong></span>.  Make
          sure the account has an SSH public/private keypair
          installed, and that the user can log in via public-key
          authentication.  Password authentication will not work,
          since all of the following SSH tricks revolve around using
          the SSH <code class="filename">authorized_keys</code> file.</p><p>If it doesn't already exist, create the
          <code class="filename">authorized_keys</code> file (on Unix,
          typically <code class="filename">~/.ssh/authorized_keys</code>).
          Each line in this file describes a public key that is
          allowed to connect.  The lines are typically of the
          form:</p><pre class="screen">
  ssh-dsa AAAABtce9euch… user@example.com
</pre><p>The first field describes the type of key, the second
          field is the uuencoded key itself, and the third field is a
          comment.  However, it's a lesser known fact that the entire
          line can be preceded by a <code class="literal">command</code>
          field:</p><pre class="screen">
  command="program" ssh-dsa AAAABtce9euch… user@example.com
</pre><p>When the <code class="literal">command</code> field is set, the
          SSH daemon will run the named program instead of the
          typical <span><strong class="command">svnserve -t</strong></span> invocation that the
          Subversion client asks for.  This opens the door to a number
          of server-side tricks.  In the following examples, we
          abbreviate the lines of the file as:</p><pre class="screen">
  command="program" TYPE KEY COMMENT
</pre></div><div class="sect3" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h4 class="title"><a id="svn.serverconfig.svnserve.sshtricks.fixedcmd"></a>Controlling the invoked command</h4></div></div></div><p>Because we can specify the executed server-side command,
          it's easy to name a specific <span><strong class="command">svnserve</strong></span>
          binary to run and to pass it extra arguments:</p><pre class="screen">
  command="/path/to/svnserve -t -r /virtual/root" TYPE KEY COMMENT
</pre><p>In this example, <code class="filename">/path/to/svnserve</code>
          might be a custom wrapper script
          around <span><strong class="command">svnserve</strong></span> which sets the umask (see
          <a href="svn.serverconfig.multimethod.html" title="Supporting Multiple Repository Access Methods">«Supporting Multiple Repository Access Methods»</a>).  It also shows how to
          anchor <span><strong class="command">svnserve</strong></span> in a virtual root
          directory, just as one often does when
          running <span><strong class="command">svnserve</strong></span> as a daemon process.
          This might be done either to restrict access to parts of the
          system, or simply to relieve the user of having to type an
          absolute path in the <code class="literal">svn+ssh://</code>
          URL.</p><p>It's also possible to have multiple users share a single
          account.  Instead of creating a separate system account for
          each user, generate a public/private keypair for each
          person.  Then place each public key into
          the <code class="filename">authorized_users</code> file, one per
          line, and use the <code class="option">--tunnel-user</code>
          option:</p><pre class="screen">
  command="svnserve -t --tunnel-user=harry" TYPE1 KEY1 harry@example.com
  command="svnserve -t --tunnel-user=sally" TYPE2 KEY2 sally@example.com
</pre><p>This example allows both Harry and Sally to connect to
          the same account via public-key authentication.  Each of
          them has a custom command that will be executed;
          the <code class="option">--tunnel-user</code> option
          tells <span><strong class="command">svnserve -t</strong></span> to assume that the named
          argument is the authenticated user.  Without
          <code class="option">--tunnel-user</code>, it would appear as though
          all commits were coming from the one shared system
          account.</p><p>A final word of caution: giving a user access to the
          server via public-key in a shared account might still allow
          other forms of SSH access, even if you've set
          the <code class="literal">command</code> value
          in <code class="filename">authorized_keys</code>.  For example, the
          user may still get shell access through SSH, or be able to
          perform X11 or general port-forwarding through your server.
          To give the user as little permission as possible, you may
          want to specify a number of restrictive options immediately
          after the <code class="literal">command</code>:</p><pre class="screen">
  command="svnserve -t --tunnel-user=harry",no-port-forwarding,\
           no-agent-forwarding,no-X11-forwarding,no-pty \
           TYPE1 KEY1 harry@example.com
</pre></div></div><div class="footnotes"><br /><hr width="100" align="left" /><div class="footnote"><p><sup>[<a id="ftn.id350701" href="#id350701">29</a>] </sup>See RFC 2195.</p></div></div></div><div class="navfooter"><hr /><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="svn.serverconfig.netmodel.html">Пред.</a> </td><td width="20%" align="center"><a accesskey="u" href="svn.serverconfig.html">Уровень выше</a></td><td width="40%" align="right"> <a accesskey="n" href="svn.serverconfig.httpd.html">След.</a></td></tr><tr><td width="40%" align="left" valign="top">Сетевая модель </td><td width="20%" align="center"><a accesskey="h" href="index.html">Начало</a></td><td width="40%" align="right" valign="top"> httpd, the Apache HTTP server</td></tr></table></div></body></html>
