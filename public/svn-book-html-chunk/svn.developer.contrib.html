<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Contributing to Subversion</title><link rel="stylesheet" href="styles.css" type="text/css" /><meta name="generator" content="DocBook XSL Stylesheets V1.71.0" /><link rel="start" href="index.html" title="Управление версиями в Subversion" /><link rel="up" href="svn.developer.html" title="Глава 8. Информация для разработчиков" /><link rel="prev" href="svn.developer.pools.html" title="Programming with Memory Pools" /><link rel="next" href="svn.ref.html" title="Глава 9. Полное справочное руководство по Subversion" /></head><body><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Contributing to Subversion</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="svn.developer.pools.html">Пред.</a> </td><th width="60%" align="center">Глава 8. Информация для разработчиков</th><td width="20%" align="right"> <a accesskey="n" href="svn.ref.html">След.</a></td></tr></table><hr /></div><div class="sect1" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a id="svn.developer.contrib"></a>Contributing to Subversion</h2></div></div></div><p>The official source of information about the Subversion
      project is, of course, the project's website at
      <a href="http://subversion.tigris.org/" target="_top">http://subversion.tigris.org/</a>.  There you can
      find information about getting access to the source code and
      participating on the discussion lists.  The Subversion community
      always welcomes new members.  If you are interested in
      participating in this community by contributing changes to the
      source code, here are some hints on how to get started.</p><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.developer.contrib.join"></a>Join the Community</h3></div></div></div><p>The first step in community participation is to find a way
        to stay on top of the latest happenings.  To do this most
        effectively, you will want to subscribe to the main developer
        discussion list (<code class="email">&lt;<a href="mailto:dev@subversion.tigris.org">dev@subversion.tigris.org</a>&gt;</code>) and
        commit mail list (<code class="email">&lt;<a href="mailto:svn@subversion.tigris.org">svn@subversion.tigris.org</a>&gt;</code>).
        By following these lists even loosely, you will have access
        to important design discussions, be able to see actual changes
        to Subversion source code as they occur, and be able to
        witness peer reviews of those changes and proposed changes.
        These email based discussion lists are the primary
        communication media for Subversion development.  See the
        Mailing Lists section of the website for other
        Subversion-related lists you might be interested in.</p><p>But how do you know what needs to be done?  It is quite
        common for a programmer to have the greatest intentions of
        helping out with the development, yet be unable to find a good
        starting point.  After all, not many folks come to the
        community having already decided on a particular itch they
        would like to scratch.  But by watching the developer
        discussion lists, you might see mentions of existing bugs or
        feature requests fly by that particularly interest you.  Also,
        a great place to look for outstanding, unclaimed tasks is the
        Issue Tracking database on the Subversion website.  There you
        will find the current list of known bugs and feature requests.
        If you want to start with something small, look for issues
        marked as «<span class="quote">bite-sized</span>».</p></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.developer.contrib.get-code"></a>Get the Source Code</h3></div></div></div><p>To edit the code, you need to have the code.  This means
        you need to check out a working copy from the public
        Subversion source repository.  As straightforward as that
        might sound, the task can be slightly tricky.  Because
        Subversion's source code is versioned using Subversion itself,
        you actually need to «<span class="quote">bootstrap</span>» by getting a
        working Subversion client via some other method.  The most
        common methods include downloading the latest binary
        distribution (if such is available for your platform), or
        downloading the latest source tarball and building your own
        Subversion client.  If you build from source, make sure to
        read the <code class="filename">INSTALL</code> file in the top level of
        the source tree for instructions.</p><p>After you have a working Subversion client, you are now
        poised to checkout a working copy of the Subversion source
        repository from <a href="http://svn.collab.net/repos/svn/trunk/" target="_top">http://svn.collab.net/repos/svn/trunk/</a>:
        <sup>[<a id="id377002" href="#ftn.id377002">51</a>]</sup></p><pre class="screen">
$ svn checkout http://svn.collab.net/repos/svn/trunk subversion
A    subversion/HACKING
A    subversion/INSTALL
A    subversion/README
A    subversion/autogen.sh
A    subversion/build.conf
…
</pre><p>The above command will checkout the bleeding-edge, latest
        version of the Subversion source code into a subdirectory
        named <code class="filename">subversion</code> in your current working
        directory.  Obviously, you can adjust that last argument as
        you see fit.  Regardless of what you call the new working copy
        directory, though, after this operation completes, you will
        now have the Subversion source code.  Of course, you will
        still need to fetch a few helper libraries (apr, apr-util,
        etc.)—see the <code class="filename">INSTALL</code> file in the
        top level of the working copy for details.</p></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.developer.contrib.hacking"></a>Become Familiar with Community Policies</h3></div></div></div><p>Now that you have a working copy containing the latest
        Subversion source code, you will most certainly want to take a
        cruise through the «<span class="quote">Hacker's Guide to Subversion</span>»,
        which is available either as the
        <code class="filename">www/hacking.html</code> file in the working copy,
        or on the Subversion website at <a href="http://subversion.tigris.org/hacking.html" target="_top">http://subversion.tigris.org/hacking.html</a>.  This guide
        contains general instructions for contributing to Subversion,
        including how to properly format your source code for
        consistency with the rest of the codebase, how to describe your
        proposed changes with an effective change log message, how to
        test your changes, and so on.  Commit privileges on the
        Subversion source repository are earned—a government by
        meritocracy.
        <sup>[<a id="id377040" href="#ftn.id377040">52</a>]</sup>
        The «<span class="quote">Hacker's Guide</span>» is an invaluable resource when
        it comes to making sure that your proposed changes earn the
        praises they deserve without being rejected on
        technicalities.</p></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.developer.contrib.code-and-test"></a>Make and Test Your Changes</h3></div></div></div><p>With the code and community policy understanding in hand,
        you are ready to make your changes.  It is best to try to make
        smaller but related sets of changes, even tackling larger
        tasks in stages, instead of making huge, sweeping
        modifications.  Your proposed changes will be easier to
        understand (and therefore easier to review) if you disturb
        the fewest lines of code possible to accomplish your task
        properly.  After making each set of proposed changes, your
        Subversion tree should be in a state in which the software
        compiles with no warnings.</p><p>Subversion has a fairly thorough
        <sup>[<a id="id377129" href="#ftn.id377129">53</a>]</sup>
        regression test suite, and your proposed changes are expected
        to not cause any of those tests to fail.  By running
        <span><strong class="command">make check</strong></span> (in Unix) from the top of the
        source tree, you can sanity-check your changes.  The fastest
        way to get your code contributions rejected (other than
        failing to supply a good log message) is to submit changes
        that cause failure in the test suite.</p><p>In the best-case scenario, you will have actually added
        appropriate tests to that test suite which verify that your
        proposed changes work as expected.  In fact,
        sometimes the best contribution a person can make is solely
        the addition of new tests.  You can write regression tests for
        functionality that currently works in Subversion as a way to
        protect against future changes that might trigger failure in
        those areas.  Also, you can write new tests that demonstrate
        known failures.  For this purpose, the Subversion test suite
        allows you to specify that a given test is expected to fail
        (called an <code class="literal">XFAIL</code>), and so long as
        Subversion fails in the way that was expected, a test result
        of <code class="literal">XFAIL</code> itself is considered a success.
        Ultimately, the better the test suite, the less time wasted on
        diagnosing potentially obscure regression bugs.</p></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.developer.contrib.submit"></a>Donate Your Changes</h3></div></div></div><p>After making your modifications to the source code,
        compose a clear and concise log message to describe those
        changes and the reasons for them.  Then, send an email to the
        developers list containing your log message and the output of
        <span><strong class="command">svn diff</strong></span> (from the top of your Subversion
        working copy).  If the community members consider your changes
        acceptable, someone who has commit privileges (permission to
        make new revisions in the Subversion source repository) will
        add your changes to the public source code tree.  Recall that
        permission to directly commit changes to the repository is
        granted on merit—if you demonstrate comprehension of
        Subversion, programming competency, and a «<span class="quote">team
        spirit</span>», you will likely be awarded that
        permission.</p></div><div class="footnotes"><br /><hr width="100" align="left" /><div class="footnote"><p><sup>[<a id="ftn.id377002" href="#id377002">51</a>] </sup>Note that the URL checked out in the example above
            ends not with <code class="literal">svn</code>, but with a
            subdirectory thereof called <code class="literal">trunk</code>.  See
            our discussion of Subversion's branching and tagging model
            for the reasoning behind this.</p></div><div class="footnote"><p><sup>[<a id="ftn.id377040" href="#id377040">52</a>] </sup>While this may superficially appear as some sort of
            elitism, this «<span class="quote">earn your commit privileges</span>»
            notion is about efficiency—whether it costs more in
            time and effort to review and apply someone else's changes
            that are likely to be safe and useful, versus the
            potential costs of undoing changes that are
            dangerous.</p></div><div class="footnote"><p><sup>[<a id="ftn.id377129" href="#id377129">53</a>] </sup>You might want to grab some popcorn.
            «<span class="quote">Thorough</span>», in this instance, translates to
            somewhere in the neighborhood of thirty minutes of
            non-interactive machine churn.</p></div></div></div><div class="navfooter"><hr /><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="svn.developer.pools.html">Пред.</a> </td><td width="20%" align="center"><a accesskey="u" href="svn.developer.html">Уровень выше</a></td><td width="40%" align="right"> <a accesskey="n" href="svn.ref.html">След.</a></td></tr><tr><td width="40%" align="left" valign="top">Programming with Memory Pools </td><td width="20%" align="center"><a accesskey="h" href="index.html">Начало</a></td><td width="40%" align="right" valign="top"> Глава 9. Полное справочное руководство по Subversion</td></tr></table></div></body></html>
