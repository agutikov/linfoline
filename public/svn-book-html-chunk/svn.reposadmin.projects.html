<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Adding Projects</title><link rel="stylesheet" href="styles.css" type="text/css" /><meta name="generator" content="DocBook XSL Stylesheets V1.71.0" /><link rel="start" href="index.html" title="Управление версиями в Subversion" /><link rel="up" href="svn.reposadmin.html" title="Глава 5. Администрирование хранилища" /><link rel="prev" href="svn.reposadmin.maint.html" title="Repository Maintenance" /><link rel="next" href="svn.reposadmin.summary.html" title="Summary" /></head><body><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Adding Projects</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="svn.reposadmin.maint.html">Пред.</a> </td><th width="60%" align="center">Глава 5. Администрирование хранилища</th><td width="20%" align="right"> <a accesskey="n" href="svn.reposadmin.summary.html">След.</a></td></tr></table><hr /></div><div class="sect1" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a id="svn.reposadmin.projects"></a>Adding Projects</h2></div></div></div><p>Once your repository is created and configured, all that
      remains is to begin using it.  If you have a collection of
      existing data that is ready to be placed under version control,
      you will more than likely want to use the <span><strong class="command">svn</strong></span>
      client program's <code class="literal">import</code> subcommand to
      accomplish that.  Before doing this, though, you should
      carefully consider your long-term plans for the repository.  In
      this section, we will offer some advice on how to plan the
      layout of your repository, and how to get your data arranged in
      that layout.</p><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.reposadmin.projects.chooselayout"></a>Choosing a Repository Layout</h3></div></div></div><p>While Subversion allows you to move around versioned files
        and directories without any loss of information, doing so can
        still disrupt the workflow of those who access the repository
        often and come to expect things to be at certain locations.
        Try to peer into the future a bit; plan ahead before placing
        your data under version control.  By «<span class="quote">laying out</span>»
        the contents of your repositories in an effective manner the
        first time, you can prevent a load of future headaches.</p><p>There are a few things to consider when setting up
        Subversion repositories.  Let's assume that as repository
        administrator, you will be responsible for supporting the
        version control system for several projects.  The first
        decision is whether to use a single repository for multiple
        projects, or to give each project its own repository, or some
        compromise of these two.</p><p>There are benefits to using a single repository for
        multiple projects, most obviously the lack of duplicated
        maintenance.  A single repository means that there is one set
        of hook scripts, one thing to routinely backup, one thing to
        dump and load if Subversion releases an incompatible new
        version, and so on.  Also, you can move data between projects
        easily, and without losing any historical versioning
        information.</p><p>The downside of using a single repository is that
        different projects may have different commit mailing lists or
        different authentication and authorization requirements.
        Also, remember that Subversion uses repository-global revision
        numbers.  Some folks don't like the fact that even though no
        changes have been made to their project lately, the youngest
        revision number for the repository keeps climbing because
        other projects are actively adding new revisions.</p><p>A middle-ground approach can be taken, too.  For example,
        projects can be grouped by how well they relate to each other.
        You might have a few repositories with a handful of projects
        in each repository.  That way, projects that are likely to
        want to share data can do so easily, and as new revisions are
        added to the repository, at least the developers know that
        those new revisions are at least remotely related to everyone
        who uses that repository.</p><p>After deciding how to organize your projects with respect
        to repositories, you'll probably want to think about directory
        hierarchies in the repositories themselves.  Because
        Subversion uses regular directory copies for branching and
        tagging (see <a href="svn.branchmerge.html" title="Глава 4. Ветвление и слияние">Глава 4, <i>Ветвление и слияние</i></a>), the Subversion
        community recommends that you choose a repository location for
        each <em class="firstterm">project root</em>—the
        «<span class="quote">top-most</span>» directory which contains data related
        to that project—and then create three subdirectories
        beneath that root: <code class="filename">trunk</code>, meaning the
        directory under which the main project development occurs;
        <code class="filename">branches</code>, which is a directory in which
        to create various named branches of the main development line;
        <code class="filename">tags</code>, which is a directory of branches
        that are created, and perhaps destroyed, but never
        changed.
        <sup>[<a id="id344165" href="#ftn.id344165">26</a>]</sup>
        </p><p>For example, your repository might look like:</p><pre class="screen">
/
   calc/
      trunk/
      tags/
      branches/
   calendar/
      trunk/
      tags/
      branches/
   spreadsheet/
      trunk/
      tags/
      branches/
   …
</pre><p>Note that it doesn't matter where in your repository each
        project root is.  If you have only one project per repository,
        the logical place to put each project root is at the root of
        that project's respective repository.  If you have multiple
        projects, you might want to arrange them in groups inside the
        repository, perhaps putting projects with similar goals or
        shared code in the same subdirectory, or maybe just grouping
        them alphabetically.  Such an arrangement might look
        like:</p><pre class="screen">
/
   utils/
      calc/
         trunk/
         tags/
         branches/
      calendar/
         trunk/
         tags/
         branches/
      …
   office/
      spreadsheet/
         trunk/
         tags/
         branches/
      …
</pre><p>Lay out your repository in whatever way you see fit.
        Subversion does not expect or enforce a layout schema—in
        its eyes, a directory is a directory is a directory.
        Ultimately, you should choose the repository arrangement that
        meets the needs of the people who work on the projects that
        live there.</p></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.reposadmin.projects.import"></a>Creating the Layout, and Importing Initial Data</h3></div></div></div><p>After deciding how to arrange the projects in your
        repository, you'll probably want to actually populate the
        repository with that layout and with initial project data.
        There are a couple of ways to do this in Subversion.  You
        could use the <span><strong class="command">svn mkdir</strong></span> command (see <a href="svn.ref.html" title="Глава 9. Полное справочное руководство по Subversion">Глава 9, <i>Полное справочное руководство по Subversion</i></a>) to create each directory in your
        skeletal repository layout, one-by-one.  A quicker way to
        accomplish the same task is to use the <span><strong class="command">svn
        import</strong></span> command (see <a href="svn.tour.other.html#svn.tour.other.import" title="svn import">«<span><strong class="command">svn import</strong></span>»</a>).  By first creating the layout
        in a temporary location on your drive, you can import the
        whole layout tree into the repository in a single
        commit:</p><pre class="screen">
$ mkdir tmpdir
$ cd tmpdir
$ mkdir projectA
$ mkdir projectA/trunk
$ mkdir projectA/branches
$ mkdir projectA/tags
$ mkdir projectB
$ mkdir projectB/trunk
$ mkdir projectB/branches
$ mkdir projectB/tags
…
$ svn import . file:///path/to/repos --message 'Initial repository layout'
Adding         projectA
Adding         projectA/trunk
Adding         projectA/branches
Adding         projectA/tags
Adding         projectB
Adding         projectB/trunk
Adding         projectB/branches
Adding         projectB/tags
…
Committed revision 1.
$ cd ..
$ rm -rf tmpdir
$
</pre><p>You can verify the results of the import by running the
        <span><strong class="command">svn list</strong></span> command:</p><pre class="screen">
$ svn list --verbose file:///path/to/repos
      1 harry               May 08 21:48 projectA/
      1 harry               May 08 21:48 projectB/
…
$
</pre><p>Once you have your skeletal layout in place, you can begin
        importing actual project data into your repository, if any
        such data exists yet.  Once again, there are several ways to
        do this.  You could use the <span><strong class="command">svn import</strong></span>
        command.  You could checkout a working copy from your new
        repository, move and arrange project data inside the working
        copy, and use the <span><strong class="command">svn add</strong></span> and <span><strong class="command">svn
        commit</strong></span> commands.  But once we start talking about
        such things, we're no longer discussing repository
        administration.  If you aren't already familiar with the
        <span><strong class="command">svn</strong></span> client program, see <a href="svn.tour.html" title="Глава 3. Экскурсия по Subversion">Глава 3, <i>Экскурсия по Subversion</i></a>.</p></div><div class="footnotes"><br /><hr width="100" align="left" /><div class="footnote"><p><sup>[<a id="ftn.id344165" href="#id344165">26</a>] </sup>The <code class="filename">trunk</code>, <code class="filename">tags</code>, 
            and <code class="filename">branches</code> trio are sometimes referred
            to as «<span class="quote">the TTB directories</span>».</p></div></div></div><div class="navfooter"><hr /><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="svn.reposadmin.maint.html">Пред.</a> </td><td width="20%" align="center"><a accesskey="u" href="svn.reposadmin.html">Уровень выше</a></td><td width="40%" align="right"> <a accesskey="n" href="svn.reposadmin.summary.html">След.</a></td></tr><tr><td width="40%" align="left" valign="top">Repository Maintenance </td><td width="20%" align="center"><a accesskey="h" href="index.html">Начало</a></td><td width="40%" align="right" valign="top"> Summary</td></tr></table></div></body></html>
