<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Using External Differencing Tools</title><link rel="stylesheet" href="styles.css" type="text/css" /><meta name="generator" content="DocBook XSL Stylesheets V1.71.0" /><link rel="start" href="index.html" title="Управление версиями в Subversion" /><link rel="up" href="svn.advanced.html" title="Глава 7. Профессиональное использование Subversion" /><link rel="prev" href="svn.advanced.l10n.html" title="Localization" /><link rel="next" href="svn.advanced.reposurls.html" title="Subversion Repository URLs" /></head><body><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Using External Differencing Tools</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="svn.advanced.l10n.html">Пред.</a> </td><th width="60%" align="center">Глава 7. Профессиональное использование Subversion</th><td width="20%" align="right"> <a accesskey="n" href="svn.advanced.reposurls.html">След.</a></td></tr></table><hr /></div><div class="sect1" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a id="svn.advanced.externaldifftools"></a>Using External Differencing Tools</h2></div></div></div><p>The presence of <code class="option">--diff-cmd</code> and
      <code class="option">--diff3-cmd</code> options, and similarly named
      runtime configuration parameters (see <a href="svn.advanced.html#svn.advanced.confarea.opts.config" title="Config">«Config»</a>), can lead to a
      false notion of how easy it is to use external differencing (or
      «<span class="quote">diff</span>») and merge tools with Subversion.  While
      Subversion can use most of popular such tools available, the
      effort invested in setting this up often turns out to be
      non-trivial.</p><p>The interface between Subversion and external diff and merge
      tools harkens back to a time when Subversion's only contextual
      differencing capabilities were built around invocations of the
      GNU diffutils toolchain, specifically the
      <span><strong class="command">diff</strong></span> and <span><strong class="command">diff3</strong></span> utilities.
      To get the kind of behavior Subversion needed, it called these
      utilities with more than a handful of options and parameters,
      most of which were quite specific to the utilities.  Some time
      later, Subversion grew its own internal differencing library,
      and as a failover mechanism,
      <sup>[<a id="id366502" href="#ftn.id366502">45</a>]</sup>
      the <code class="option">--diff-cmd</code> and <code class="option">--diff3-cmd</code>
      options were added to the Subversion command-line client so
      users could more easily indicate that they preferred to use the
      GNU diff and diff3 utilities instead of the newfangled internal
      diff library.  If those options were used, Subversion would
      simply ignore the internal diff library, and fall back to
      running those external programs, lengthy argument lists and all.
      And that's where things remain today.</p><p>It didn't take long for folks to realize that having such
      easy configuration mechanisms for specifying that Subversion
      should use the external GNU diff and diff3 utilities located at
      a particular place on the system could be applied toward the use
      of other diff and merge tools, too.  After all, Subversion
      didn't actually verify that the things it was being told to run
      were members of the GNU diffutils toolchain.  But the only
      configurable aspect of using those external tools is their
      location on the system—not the option set, parameter
      order, etc.  Subversion continues throwing all those GNU utility
      options at your external diff tool regardless of whether or not
      that program can understand those options.  And that's where
      things get unintuitive for most users.</p><p>The key to using external diff and merge tools (other than
      GNU diff and diff3, of course) with Subversion is to use wrapper
      scripts which convert the input from Subversion into something
      that your differencing tool can understand, and then to convert
      the output of your tool back into a format which Subversion
      expects—the format that the GNU tools would have used.
      The following sections cover the specifics of those
      expectations.</p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Замечание</h3><p>The decision on when to fire off a contextual diff or
        merge as part of a larger Subversion operation is made
        entirely by Subversion, and is affected by, among other
        things, whether or not the files being operated on are
        human-readable as determined by their
        <code class="literal">svn:mime-type</code> property.  This means, for
        example, that even if you had the niftiest Microsoft
        Word-aware differencing or merging tool in the Universe, it
        would never be invoked by Subversion so long as your versioned
        Word documents had a configured MIME type that denoted that
        they were not human-readable (such as
        <code class="literal">application/msword</code>).  For more about MIME
        type settings, see <a href="svn.advanced.props.html#svn.advanced.props.special.mime-type" title="svn:mime-type">«<code class="literal">svn:mime-type</code>»</a></p></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.advanced.externaldifftools.diff"></a>External diff</h3></div></div></div><p>Subversion calls external diff programs with parameters
        suitable for the GNU diff utility, and expects only that the
        external program return with a successful error code.  For
        most alternative diff program, only the sixth and seventh
        arguments, the paths of the files which represent the left and
        right sides of the diff, respectively, are of interest.  Note
        that Subversion runs the diff program once per modified file
        covered by the Subversion operation, so if your program runs
        in an asynchronous fashion (or «<span class="quote">backgrounded</span>»),
        you might have several instances of it all running
        simultaneously.  Finally, Subversion expects that your program
        return an errorcode of 1 if your program detected differences,
        or 0 if it did not—any other errorcode is considered a
        fatal error.
        <sup>[<a id="id366526" href="#ftn.id366526">46</a>]</sup>
      </p><p><a href="svn.advanced.externaldifftools.html#svn.advanced.externaldifftools.diff.ex-1" title="Пример 7.2. diffwrap.sh">Пример 7.2, «diffwrap.sh»</a>
        and <a href="svn.advanced.externaldifftools.html#svn.advanced.externaldifftools.diff.ex-2" title="Пример 7.3. diffwrap.bat">Пример 7.3, «diffwrap.bat»</a>
        are templates for external diff tool wrappers in the Bourne
        shell and Windows batch scripting languages,
        respectively.</p><div class="example"><a id="svn.advanced.externaldifftools.diff.ex-1"></a><p class="title"><b>Пример 7.2. diffwrap.sh</b></p><div class="example-contents"><pre class="programlisting">
#!/bin/sh

# Configure your favorite diff program here.
DIFF="/usr/local/bin/my-diff-tool"

# Subversion provides the paths we need as the sixth and seventh
# parameters.
LEFT=${6}
RIGHT=${7}

# Call the diff command (change the following line to make sense for
# your merge program).
$DIFF --left $LEFT --right $RIGHT

# Return an errorcode of 0 if no differences were detected, 1 if some were.
# Any other errorcode will be treated as fatal.
</pre></div></div><br class="example-break" /><div class="example"><a id="svn.advanced.externaldifftools.diff.ex-2"></a><p class="title"><b>Пример 7.3. diffwrap.bat</b></p><div class="example-contents"><pre class="programlisting">
@ECHO OFF

REM Configure your favorite diff program here.
SET DIFF="C:\Program Files\Funky Stuff\My Diff Tool.exe"

REM Subversion provides the paths we need as the sixth and seventh
REM parameters.
SET LEFT=%6
SET RIGHT=%7

REM Call the diff command (change the following line to make sense for
REM your merge program).
%DIFF% --left %LEFT% --right %RIGHT%

REM Return an errorcode of 0 if no differences were detected, 1 if some were.
REM Any other errorcode will be treated as fatal.
</pre></div></div><br class="example-break" /></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.advanced.externaldifftools.diff3"></a>External diff3</h3></div></div></div><p>Subversion calls external merge programs with parameters
        suitable for the GNU diff3 utility, expecting that the
        external program return with a successful error code and that
        the full file contents which result from the completed merge
        operation are printed on the standard output stream (so that
        Subversion can redirect them into the appropriate version
        controlled file).  For most alternative merge programs, only
        the ninth, tenth, and eleventh arguments, the paths of the
        files which represent the «<span class="quote">mine</span>»,
        «<span class="quote">older</span>», and «<span class="quote">yours</span>» inputs,
        respectively, are of interest.  Note that because Subversion
        depends on the output of your merge program, you wrapper
        script must not exit before that output has been delivered to
        Subversion.  When it finally does exit, it should return an
        errorcode of 0 if the merge was successful, or 1 if unresolved
        conflicts remain in the output—any other errorcode is
        considered a fatal error.</p><p><a href="svn.advanced.externaldifftools.html#svn.advanced.externaldifftools.diff3.ex-1" title="Пример 7.4. diff3wrap.sh">Пример 7.4, «diff3wrap.sh»</a>
        and <a href="svn.advanced.externaldifftools.html#svn.advanced.externaldifftools.diff3.ex-2" title="Пример 7.5. diff3wrap.bat">Пример 7.5, «diff3wrap.bat»</a> are
        templates for external merge tool wrappers in the Bourne shell
        and Windows batch scripting languages, respectively.</p><div class="example"><a id="svn.advanced.externaldifftools.diff3.ex-1"></a><p class="title"><b>Пример 7.4. diff3wrap.sh</b></p><div class="example-contents"><pre class="programlisting">
#!/bin/sh

# Configure your favorite diff3/merge program here.
DIFF3="/usr/local/bin/my-merge-tool"

# Subversion provides the paths we need as the ninth, tenth, and eleventh
# parameters.
MINE=${9}
OLDER=${10}
YOURS=${11}

# Call the merge command (change the following line to make sense for
# your merge program).
$DIFF3 --older $OLDER --mine $MINE --yours $YOURS

# After performing the merge, this script needs to print the contents
# of the merged file to stdout.  Do that in whatever way you see fit.
# Return an errorcode of 0 on successful merge, 1 if unresolved conflicts
# remain in the result.  Any other errorcode will be treated as fatal.
</pre></div></div><br class="example-break" /><div class="example"><a id="svn.advanced.externaldifftools.diff3.ex-2"></a><p class="title"><b>Пример 7.5. diff3wrap.bat</b></p><div class="example-contents"><pre class="programlisting">
@ECHO OFF

REM Configure your favorite diff3/merge program here.
SET DIFF3="C:\Program Files\Funky Stuff\My Merge Tool.exe"

REM Subversion provides the paths we need as the ninth, tenth, and eleventh
REM parameters.  But we only have access to nine parameters at a time, so we
REM shift our nine-parameter window twice to let us get to what we need.
SHIFT
SHIFT
SET MINE=%7
SET OLDER=%8
SET YOURS=%9

REM Call the merge command (change the following line to make sense for
REM your merge program).
%DIFF3% --older %OLDER% --mine %MINE% --yours %YOURS%

REM After performing the merge, this script needs to print the contents
REM of the merged file to stdout.  Do that in whatever way you see fit.
REM Return an errorcode of 0 on successful merge, 1 if unresolved conflicts
REM remain in the result.  Any other errorcode will be treated as fatal.
</pre></div></div><br class="example-break" /></div><div class="footnotes"><br /><hr width="100" align="left" /><div class="footnote"><p><sup>[<a id="ftn.id366502" href="#id366502">45</a>] </sup>Subversion developers are good, but even the best make
          mistakes.</p></div><div class="footnote"><p><sup>[<a id="ftn.id366526" href="#id366526">46</a>] </sup>The GNU diff manual page puts it this way: «<span class="quote">An
            exit status of 0 means no differences were found, 1 means some
            differences were found, and 2 means trouble.</span>»</p></div></div></div><div class="navfooter"><hr /><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="svn.advanced.l10n.html">Пред.</a> </td><td width="20%" align="center"><a accesskey="u" href="svn.advanced.html">Уровень выше</a></td><td width="40%" align="right"> <a accesskey="n" href="svn.advanced.reposurls.html">След.</a></td></tr><tr><td width="40%" align="left" valign="top">Localization </td><td width="20%" align="center"><a accesskey="h" href="index.html">Начало</a></td><td width="40%" align="right" valign="top"> Subversion Repository URLs</td></tr></table></div></body></html>
