<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Supporting Multiple Repository Access Methods</title><link rel="stylesheet" href="styles.css" type="text/css" /><meta name="generator" content="DocBook XSL Stylesheets V1.71.0" /><link rel="start" href="index.html" title="Управление версиями в Subversion" /><link rel="up" href="svn.serverconfig.html" title="Глава 6. Настройка сервера" /><link rel="prev" href="svn.serverconfig.httpd.html" title="httpd, the Apache HTTP server" /><link rel="next" href="svn.advanced.html" title="Глава 7. Профессиональное использование Subversion" /></head><body><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Supporting Multiple Repository Access Methods</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="svn.serverconfig.httpd.html">Пред.</a> </td><th width="60%" align="center">Глава 6. Настройка сервера</th><td width="20%" align="right"> <a accesskey="n" href="svn.advanced.html">След.</a></td></tr></table><hr /></div><div class="sect1" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a id="svn.serverconfig.multimethod"></a>Supporting Multiple Repository Access Methods</h2></div></div></div><p>You've seen how a repository can be accessed in many
      different ways.  But is it possible—or safe—for your
      repository to be accessed by multiple methods simultaneously?
      The answer is yes, provided you use a bit of foresight.</p><p>At any given time, these processes may require read and
      write access to your repository:</p><div class="itemizedlist"><ul type="disc"><li><p>regular system users using a Subversion client (as
          themselves) to access the repository directly via
          <code class="literal">file:///</code> URLs;</p></li><li><p>regular system users connecting to SSH-spawned private
          <span><strong class="command">svnserve</strong></span> processes (running as
          themselves) which access the repository;</p></li><li><p>an <span><strong class="command">svnserve</strong></span> process—either a
          daemon or one launched by
          <span><strong class="command">inetd</strong></span>—running as a particular fixed
          user;</p></li><li><p>an Apache <span><strong class="command">httpd</strong></span> process, running as a
          particular fixed user.</p></li></ul></div><p>The most common problem administrators run into is repository
      ownership and permissions.  Does every process (or user) in the
      previous list have the rights to read and write the Berkeley DB
      files?  Assuming you have a Unix-like operating system, a
      straightforward approach might be to place every potential
      repository user into a new <code class="literal">svn</code> group, and
      make the repository wholly owned by that group.  But even that's
      not enough, because a process may write to the database files
      using an unfriendly umask—one that prevents access by
      other users.</p><p>So the next step beyond setting up a common group for
      repository users is to force every repository-accessing process
      to use a sane umask.  For users accessing the repository
      directly, you can make the <span><strong class="command">svn</strong></span> program into a
      wrapper script that first sets <span><strong class="command">umask 002</strong></span> and
      then runs the real <span><strong class="command">svn</strong></span> client program.  You
      can write a similar wrapper script for the
      <span><strong class="command">svnserve</strong></span> program, and add a <span><strong class="command">umask
      002</strong></span> command to Apache's own startup script,
      <code class="filename">apachectl</code>.  For example:</p><pre class="screen">
$ cat /usr/bin/svn

#!/bin/sh

umask 002
/usr/bin/svn-real "$@"

</pre><p>Another common problem is often encountered on Unix-like
      systems.  As a repository is used, Berkeley DB occasionally
      creates new log files to journal its actions.  Even if the
      repository is wholly owned by the <span><strong class="command">svn</strong></span> group,
      these newly created files won't necessarily be owned by that
      same group, which then creates more permissions problems for
      your users.  A good workaround is to set the group SUID bit on
      the repository's <code class="filename">db</code> directory.  This causes
      all newly-created log files to have the same group owner as the
      parent directory.</p><p>Once you've jumped through these hoops, your repository
      should be accessible by all the necessary processes.  It may
      seem a bit messy and complicated, but the problems of having
      multiple users sharing write-access to common files are classic
      ones that are not often elegantly solved.</p><p>Fortunately, most repository administrators will never
      <span class="emphasis"><em>need</em></span> to have such a complex configuration.
      Users who wish to access repositories that live on the same
      machine are not limited to using <code class="literal">file://</code>
      access URLs—they can typically contact the Apache HTTP
      server or <span><strong class="command">svnserve</strong></span> using
      <code class="literal">localhost</code> for the server name in their
      <code class="literal">http://</code> or <code class="literal">svn://</code> URLs.
      And to maintain multiple server processes for your Subversion
      repositories is likely to be more of a headache than necessary.
      We recommend you choose the server that best meets your needs
      and stick with it!</p><div class="sidebar"><p class="title"><b>The svn+ssh:// server checklist</b></p><p>It can be quite tricky to get a bunch of users with
        existing SSH accounts to share a repository without
        permissions problems.  If you're confused about all the things
        that you (as an administrator) need to do on a Unix-like
        system, here's a quick checklist that resummarizes some of
        things discussed in this section:</p><div class="itemizedlist"><ul type="disc"><li><p>All of your SSH users need to be able to read and
            write to the repository.  Put all the SSH users into a
            single group.  Make the repository wholly owned by that
            group, and set the group permissions to read/write.</p></li><li><p>Your users need to use a sane umask when accessing the
            repository.  Make sure that <span><strong class="command">svnserve</strong></span>
            (<code class="filename">/usr/bin/svnserve</code>, or wherever
            it lives in <code class="literal">$PATH</code>) is actually a
            wrapper script which sets <span><strong class="command">umask 002</strong></span> and
            executes the real <span><strong class="command">svnserve</strong></span>
            binary.  Take similar measures when using
            <span><strong class="command">svnlook</strong></span> and
            <span><strong class="command">svnadmin</strong></span>.  Either run them with a sane
            umask, or wrap them as described above.</p></li></ul></div></div></div><div class="navfooter"><hr /><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="svn.serverconfig.httpd.html">Пред.</a> </td><td width="20%" align="center"><a accesskey="u" href="svn.serverconfig.html">Уровень выше</a></td><td width="40%" align="right"> <a accesskey="n" href="svn.advanced.html">След.</a></td></tr><tr><td width="40%" align="left" valign="top">httpd, the Apache HTTP server </td><td width="20%" align="center"><a accesskey="h" href="index.html">Начало</a></td><td width="40%" align="right" valign="top"> Глава 7. Профессиональное использование Subversion</td></tr></table></div></body></html>
