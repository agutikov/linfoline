<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Inside the Working Copy Administration Area</title><link rel="stylesheet" href="styles.css" type="text/css" /><meta name="generator" content="DocBook XSL Stylesheets V1.71.0" /><link rel="start" href="index.html" title="Управление версиями в Subversion" /><link rel="up" href="svn.developer.html" title="Глава 8. Информация для разработчиков" /><link rel="prev" href="svn.developer.usingapi.html" title="Using the APIs" /><link rel="next" href="svn.developer.webdav.html" title="WebDAV" /></head><body><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Inside the Working Copy Administration Area</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="svn.developer.usingapi.html">Пред.</a> </td><th width="60%" align="center">Глава 8. Информация для разработчиков</th><td width="20%" align="right"> <a accesskey="n" href="svn.developer.webdav.html">След.</a></td></tr></table><hr /></div><div class="sect1" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a id="svn.developer.insidewc"></a>Inside the Working Copy Administration Area</h2></div></div></div><p>As we mentioned earlier, each directory of a Subversion
      working copy contains a special subdirectory called
      <code class="filename">.svn</code> which houses administrative data about
      that working copy directory.  Subversion uses the information in
      <code class="filename">.svn</code> to keep track of things like:</p><div class="itemizedlist"><ul type="disc"><li><p>Which repository location(s) are represented by the
          files and subdirectories in the working copy
          directory.</p></li><li><p>What revision of each of those files and directories are
          currently present in the working copy.</p></li><li><p>Any user-defined properties that might be attached
          to those files and directories.</p></li><li><p>Pristine (un-edited) copies of the working copy
          files.</p></li></ul></div><p>While there are several other bits of data stored in the
      <code class="filename">.svn</code> directory, we will examine only a
      couple of the most important items.</p><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.developer.insidewc.entries"></a>The Entries File</h3></div></div></div><p>Perhaps the single most important file in the
        <code class="filename">.svn</code> directory is the
        <code class="filename">entries</code> file.  The entries file is an XML
        document which contains the bulk of the administrative
        information about a versioned resource in a working copy
        directory.  It is this one file which tracks the repository
        URLs, pristine revision, file checksums, pristine text and
        property timestamps, scheduling and conflict state
        information, last-known commit information (author, revision,
        timestamp), local copy history—practically everything
        that a Subversion client is interested in knowing about a
        versioned (or to-be-versioned) resource!</p><div class="sidebar"><p class="title"><b>Comparing the Administrative Areas of Subversion and
          CVS</b></p><p>A glance inside the typical <code class="filename">.svn</code>
          directory turns up a bit more than what CVS maintains in its
          <code class="filename">CVS</code> administrative directories.  The
          <code class="filename">entries</code> file contains XML which
          describes the current state of the working copy directory,
          and basically serves the purposes of CVS's
          <code class="filename">Entries</code>, <code class="filename">Root</code>, and
          <code class="filename">Repository</code> files combined.</p></div><p>The following is an example of an actual entries
        file:</p><div class="example"><a id="svn.developer.insidewc.entries.ex-1"></a><p class="title"><b>Пример 8.4. Contents of a Typical <code class="filename">.svn/entries</code>
          File</b></p><div class="example-contents"><pre class="programlisting">
&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;wc-entries
   xmlns="svn:"&gt;
&lt;entry
   committed-rev="1"
   name=""
   committed-date="2005-04-04T13:32:28.526873Z"
   url="http://svn.red-bean.com/repos/greek-tree/A/D"
   last-author="jrandom"
   kind="dir"
   uuid="4e820d15-a807-0410-81d5-aa59edf69161"
   revision="1"/&gt;
&lt;entry
   name="lambda"
   copied="true"
   kind="file"
   copyfrom-rev="1"
   schedule="add"
   copyfrom-url="http://svn.red-bean.com/repos/greek-tree/A/B/lambda"/&gt;
&lt;entry
   committed-rev="1"
   name="gamma"
   text-time="2005-12-11T16:32:46.000000Z"
   committed-date="2005-04-04T13:32:28.526873Z"
   checksum="ada10d942b1964d359e048dbacff3460"
   last-author="jrandom"
   kind="file"
   prop-time="2005-12-11T16:32:45.000000Z"/&gt;
&lt;entry
   name="zeta"
   kind="file"
   schedule="add"
   revision="0"/&gt;
&lt;entry
   name="G"
   kind="dir"/&gt;
&lt;entry
   name="H"
   kind="dir"
   schedule="delete"/&gt;
&lt;/wc-entries&gt;
</pre></div></div><br class="example-break" /><p>As you can see, the entries file is essentially a list of
        entries.  Each <code class="sgmltag-element">entry</code> tag represents one of
        three things: the working copy directory itself (called the
        «<span class="quote">this directory</span>» entry, and noted as having an
        empty value for its <em class="structfield"><code>name</code></em>
        attribute), a file in that working copy directory (noted by
        having its <em class="structfield"><code>kind</code></em> attribute set to
        <code class="literal">"file"</code>), or a subdirectory in that working
        copy (<em class="structfield"><code>kind</code></em> here is set to
        <code class="literal">"dir"</code>).  The files and subdirectories whose
        entries are stored in this file are either already under
        version control, or (as in the case of the file named
        <code class="filename">zeta</code> above) are scheduled to be added to
        version control when the user next commits this working copy
        directory's changes.  Each entry has a unique name, and each
        entry has a node kind.</p><p>Developers should be aware of some special rules that
        Subversion uses when reading and writing its
        <code class="filename">entries</code> files.  While each entry has a
        revision and URL associated with it, note that not every
        <code class="sgmltag-element">entry</code> tag in the sample file has explicit
        <em class="structfield"><code>revision</code></em> or
        <em class="structfield"><code>url</code></em> attributes attached to it.
        Subversion allows entries to not explicitly store those two
        attributes when their values are the same as (in the
        <em class="structfield"><code>revision</code></em> case) or trivially
        calculable from
        <sup>[<a id="id376466" href="#ftn.id376466">50</a>]</sup>
        (in the <em class="structfield"><code>url</code></em> case) the data stored
        in the «<span class="quote">this directory</span>» entry.  Note also that
        for subdirectory entries, Subversion stores only the crucial
        attributes—name, kind, url, revision, and schedule.  In
        an effort to reduce duplicated information, Subversion
        dictates that the method for determining the full set of
        information about a subdirectory is to traverse down into that
        subdirectory, and read the «<span class="quote">this directory</span>» entry
        from its own <code class="filename">.svn/entries</code> file.  However,
        a reference to the subdirectory is kept in its parent's
        <code class="filename">entries</code> file, with enough information to
        permit basic versioning operations in the event that the
        subdirectory itself is actually missing from disk.</p></div><div class="sect2" lang="ru" xml:lang="ru"><div class="titlepage"><div><div><h3 class="title"><a id="svn.developer.insidewc.base-and-props"></a>Pristine Copies and Property Files</h3></div></div></div><p>As mentioned before, the <code class="filename">.svn</code>
        directory also holds the pristine «<span class="quote">text-base</span>»
        versions of files.  Those can be found in
        <code class="filename">.svn/text-base</code>.  The benefits of these
        pristine copies are multiple—network-free checks for
        local modifications and difference reporting, network-free
        reversion of modified or missing files, smaller transmission
        of changes to the server—but comes at the cost of having
        each versioned file stored at least twice on disk.  These
        days, this seems to be a negligible penalty for most files.
        However, the situation gets uglier as the size of your
        versioned files grows.  Some attention is being given to
        making the presence of the «<span class="quote">text-base</span>» an option.
        Ironically though, it is as your versioned files' sizes get
        larger that the existence of the «<span class="quote">text-base</span>»
        becomes more crucial—who wants to transmit a huge file
        across a network just because they want to commit a tiny
        change to it?</p><p>Similar in purpose to the «<span class="quote">text-base</span>» files
        are the property files and their pristine
        «<span class="quote">prop-base</span>» copies, located in
        <code class="filename">.svn/props</code> and
        <code class="filename">.svn/prop-base</code> respectively.  Since
        directories can have properties, too, there are also
        <code class="filename">.svn/dir-props</code> and
        <code class="filename">.svn/dir-prop-base</code> files.  Each of these
        property files («<span class="quote">working</span>» and «<span class="quote">base</span>»
        versions) uses a simple «<span class="quote">hash-on-disk</span>» file
        format for storing the property names and values.</p></div><div class="footnotes"><br /><hr width="100" align="left" /><div class="footnote"><p><sup>[<a id="ftn.id376466" href="#id376466">50</a>] </sup>That is, the URL for the entry is the same as the
            concatenation of the parent directory's URL and the
            entry's name.</p></div></div></div><div class="navfooter"><hr /><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="svn.developer.usingapi.html">Пред.</a> </td><td width="20%" align="center"><a accesskey="u" href="svn.developer.html">Уровень выше</a></td><td width="40%" align="right"> <a accesskey="n" href="svn.developer.webdav.html">След.</a></td></tr><tr><td width="40%" align="left" valign="top">Using the APIs </td><td width="20%" align="center"><a accesskey="h" href="index.html">Начало</a></td><td width="40%" align="right" valign="top"> WebDAV</td></tr></table></div></body></html>
