<!-- �������������, ��������� � ������������� ����� ��� HTML -->

<!-- ����� ��������. �������� �����:
     <!ENTITY % HTMLsymbol PUBLIC
       "-//W3C//ENTITIES Symbols//EN//HTML">
     %HTMLsymbol; -->

<!-- Portions � International Organization for Standardization 1986
    ���������� �� ����������� � ����� ����� ���� ��� ������������� �
    ���������������� ��������� SGML � ������������, ��� ���������� �
    ISO 8879, ��� �������, ��� ������ ���������� �������� �� ��� �����.
-->

<!-- ��������������� ����� �������� ISO �����, ���� ����� �������� �� ���������.
����� ����� (�.�., ��� ������ ISO 8879) �� ����������� � ����� ������������ � ISO 8879 ������ ���������. 
�������� ��������� ISO 10646 ���� ��� ������� ������� � 16-������ 
����. �������� CDATA - ������������ ��������� �������� ISO 10646 � 
��������� �� ����� �������� ���������. ����� �������� ������� ISO 10646.

-->

<!-- Latin Extended-B -->
<!ENTITY fnof     CDATA "&#402;" -- ��������� ����� f � ������� = �������, U+0192 ISOtech -->

<!-- Greek -->
<!ENTITY Alpha    CDATA "&#913;" -- ��������� ��������� alpha, U+0391 -->
<!ENTITY Beta     CDATA "&#914;" -- ��������� ��������� beta, U+0392 -->
<!ENTITY Gamma    CDATA "&#915;" -- ��������� ��������� gamma, U+0393 ISOgrk3 -->
<!ENTITY Delta    CDATA "&#916;" -- ��������� ��������� delta, U+0394 ISOgrk3 -->
<!ENTITY Epsilon  CDATA "&#917;" -- ��������� ��������� epsilon, U+0395 -->
<!ENTITY Zeta     CDATA "&#918;" -- ��������� ��������� zeta, U+0396 -->
<!ENTITY Eta      CDATA "&#919;" -- ��������� ��������� eta, U+0397 -->
<!ENTITY Theta    CDATA "&#920;" -- ��������� ��������� theta, U+0398 ISOgrk3 -->
<!ENTITY Iota     CDATA "&#921;" -- ��������� ��������� iota, U+0399 -->
<!ENTITY Kappa    CDATA "&#922;" -- ��������� ��������� kappa, U+039A -->
<!ENTITY Lambda   CDATA "&#923;" -- ��������� ��������� lambda, U+039B ISOgrk3 -->
<!ENTITY Mu       CDATA "&#924;" -- ��������� ��������� mu, U+039C -->
<!ENTITY Nu       CDATA "&#925;" -- ��������� ��������� nu, U+039D -->
<!ENTITY Xi       CDATA "&#926;" -- ��������� ��������� xi, U+039E ISOgrk3 -->
<!ENTITY Omicron  CDATA "&#927;" -- ��������� ��������� omicron, U+039F -->
<!ENTITY Pi       CDATA "&#928;" -- ��������� ��������� pi, U+03A0 ISOgrk3 -->
<!ENTITY Rho      CDATA "&#929;" -- ��������� ��������� rho, U+03A1 -->
<!-- ����������� ������� Sigmaf � U+03A2 -->
<!ENTITY Sigma    CDATA "&#931;" -- ��������� ��������� sigma, U+03A3 ISOgrk3 -->
<!ENTITY Tau      CDATA "&#932;" -- ��������� ��������� tau, U+03A4 -->
<!ENTITY Upsilon  CDATA "&#933;" -- ��������� ��������� upsilon, U+03A5 ISOgrk3 -->
<!ENTITY Phi      CDATA "&#934;" -- ��������� ��������� phi, U+03A6 ISOgrk3 -->
<!ENTITY Chi      CDATA "&#935;" -- ��������� ��������� chi, U+03A7 -->
<!ENTITY Psi      CDATA "&#936;" -- ��������� ��������� psi, U+03A8 ISOgrk3 -->
<!ENTITY Omega    CDATA "&#937;" -- ��������� ��������� omega, U+03A9 ISOgrk3 -->

<!ENTITY alpha    CDATA "&#945;" -- ��������� alpha, U+03B1 ISOgrk3 -->
<!ENTITY beta     CDATA "&#946;" -- ��������� beta, U+03B2 ISOgrk3 -->
<!ENTITY gamma    CDATA "&#947;" -- ��������� gamma, U+03B3 ISOgrk3 -->
<!ENTITY delta    CDATA "&#948;" -- ��������� delta, U+03B4 ISOgrk3 -->
<!ENTITY epsilon  CDATA "&#949;" -- ��������� epsilon, U+03B5 ISOgrk3 -->
<!ENTITY zeta     CDATA "&#950;" -- ��������� zeta, U+03B6 ISOgrk3 -->
<!ENTITY eta      CDATA "&#951;" -- ��������� eta, U+03B7 ISOgrk3 -->
<!ENTITY theta    CDATA "&#952;" -- ��������� theta, U+03B8 ISOgrk3 -->
<!ENTITY iota     CDATA "&#953;" -- ��������� iota, U+03B9 ISOgrk3 -->
<!ENTITY kappa    CDATA "&#954;" -- ��������� kappa, U+03BA ISOgrk3 -->
<!ENTITY lambda   CDATA "&#955;" -- ��������� lambda, U+03BB ISOgrk3 -->
<!ENTITY mu       CDATA "&#956;" -- ��������� mu, U+03BC ISOgrk3 -->
<!ENTITY nu       CDATA "&#957;" -- ��������� nu, U+03BD ISOgrk3 -->
<!ENTITY xi       CDATA "&#958;" -- ��������� xi, U+03BE ISOgrk3 -->
<!ENTITY omicron  CDATA "&#959;" -- ��������� omicron, U+03BF NEW -->
<!ENTITY pi       CDATA "&#960;" -- ��������� pi, U+03C0 ISOgrk3 -->
<!ENTITY rho      CDATA "&#961;" -- ��������� rho, U+03C1 ISOgrk3 -->
<!ENTITY sigmaf   CDATA "&#962;" -- ��������� final sigma, U+03C2 ISOgrk3 -->
<!ENTITY sigma    CDATA "&#963;" -- ��������� sigma, U+03C3 ISOgrk3 -->
<!ENTITY tau      CDATA "&#964;" -- ��������� tau, U+03C4 ISOgrk3 -->
<!ENTITY upsilon  CDATA "&#965;" -- ��������� upsilon, U+03C5 ISOgrk3 -->
<!ENTITY phi      CDATA "&#966;" -- ��������� phi, U+03C6 ISOgrk3 -->
<!ENTITY chi      CDATA "&#967;" -- ��������� chi, U+03C7 ISOgrk3 -->
<!ENTITY psi      CDATA "&#968;" -- ��������� psi, U+03C8 ISOgrk3 -->
<!ENTITY omega    CDATA "&#969;" -- ��������� omega, U+03C9 ISOgrk3 -->
<!ENTITY thetasym CDATA "&#977;" -- ���������r theta symbol, U+03D1 NEW -->
<!ENTITY upsih    CDATA "&#978;" -- ��������� upsilon symbol � �������, U+03D2 NEW -->
<!ENTITY piv      CDATA "&#982;" -- ��������� pi symbol, U+03D6 ISOgrk3 -->

<!-- ����� ���������� -->
<!ENTITY bull     CDATA "&#8226;" -- ������ = ������ ������, U+2022 ISOpub  -->
<!-- bull ��� �� �� ��, ��� �������� bullet, U+2219 -->
<!ENTITY hellip   CDATA "&#8230;" -- ����������, U+2026 ISOpub  -->
<!ENTITY prime    CDATA "&#8242;" -- ������ = ���, U+2032 ISOtech -->
<!ENTITY Prime    CDATA "&#8243;" -- ������� = ����, U+2033 ISOtech -->
<!ENTITY oline    CDATA "&#8254;" -- ������� ������ �������������, U+203E NEW -->
<!ENTITY frasl    CDATA "&#8260;" -- ���� - �������, U+2044 NEW -->

<!-- ������������� ������� -->
<!ENTITY weierp   CDATA "&#8472;" -- script ��������� P, U+2118 ISOamso -->
<!ENTITY image    CDATA "&#8465;" -- ������ ��������� I, U+2111 ISOamso -->
<!ENTITY real     CDATA "&#8476;" -- ������ ��������� R, U+211C ISOamso -->
<!ENTITY trade    CDATA "&#8482;" -- trade mark, U+2122 ISOnum -->
<!ENTITY alefsym  CDATA "&#8501;" -- ������ alef, U+2135 NEW -->
<!-- ������ alef symbol ��� �� �� ��, ��� ��������� ����� alef,
     U+05D0, ���� ��� ��� ������� ����� ������������ ����� ������ -->

<!-- ������� -->
<!ENTITY larr     CDATA "&#8592;" -- �����, U+2190 ISOnum -->
<!ENTITY uarr     CDATA "&#8593;" -- �����, U+2191 ISOnum-->
<!ENTITY rarr     CDATA "&#8594;" -- ������, U+2192 ISOnum -->
<!ENTITY darr     CDATA "&#8595;" -- ����, U+2193 ISOnum -->
<!ENTITY harr     CDATA "&#8596;" -- �����-������, U+2194 ISOamsa -->
<!ENTITY crarr    CDATA "&#8629;" -- ���� � ����� ����� = ������� �������, U+21B5 NEW -->
<!ENTITY lArr     CDATA "&#8656;" -- ����� �������, U+21D0 ISOtech -->
<!-- � ISO 10646 �� ���������, ��� lArr - ��� �� ��, ��� � 
������� '���������������', �� ����� ��� ������� ������� ��� 
���� �������. ��� ��� ? lArr ����� �������������� ��� '���������������', 
��� ���������� ISOtech -->
<!ENTITY uArr     CDATA "&#8657;" -- ����� �������, U+21D1 ISOamsa -->
<!ENTITY rArr     CDATA "&#8658;" -- ������ �������, U+21D2 ISOtech -->
<!ENTITY dArr     CDATA "&#8659;" -- ���� �������, U+21D3 ISOamsa -->
<!ENTITY hArr     CDATA "&#8660;" -- ����� �������, U+21D4 ISOamsa -->

<!-- �������������� ��������� -->
<!ENTITY forall   CDATA "&#8704;" -- ��� ����, U+22��00 ISOtech -->
<!ENTITY part     CDATA "&#8706;" -- ��������� ������������, U+2202 ISOtech  -->
<!ENTITY exist    CDATA "&#8707;" -- ����������, U+2203 ISOtech -->
<!ENTITY empty    CDATA "&#8709;" -- ������ ����� = �������, U+2205 ISOamso -->
<!ENTITY nabla    CDATA "&#8711;" -- nabla = backward difference, U+2207 ISOtech -->
<!ENTITY isin     CDATA "&#8712;" -- ������� ��, U+2208 ISOtech -->
<!ENTITY notin    CDATA "&#8713;" -- �� ������� ��, U+2209 ISOtech -->
<!ENTITY ni       CDATA "&#8715;" -- �������� ��� ����, U+220B ISOtech -->
<!-- ������� ������ ���� ����� �������������� ��������, ��� 'ni'? -->
<!ENTITY prod     CDATA "&#8719;" -- n-ary product = product sign, U+220F ISOamsb -->
<!-- prod prod ��� �� �� ��, ��� U+03A0 '��������� ��������� pi', ���� �
     ����� ���� ����������� ���� ���� ��� ����������� ����� �������� -->
<!ENTITY sum      CDATA "&#8721;" -- n-ary sumation, U+2211 ISOamsb -->
<!-- ��� �� �� ��, ��� U+03A3 '��������� ��������� sigma', ���� �
     ����� ���� ����������� ���� ���� ��� ����������� ����� �������� -->
<!ENTITY minus    CDATA "&#8722;" -- �����, U+2212 ISOtech -->
<!ENTITY lowast   CDATA "&#8727;" -- �������� ��������, U+2217 ISOtech -->
<!ENTITY radic    CDATA "&#8730;" -- ���������� ������, U+221A ISOtech -->
<!ENTITY prop     CDATA "&#8733;" -- ��������������� �, U+221D ISOtech -->
<!ENTITY infin    CDATA "&#8734;" -- �������������, U+221E ISOtech -->
<!ENTITY ang      CDATA "&#8736;" -- ����, U+2220 ISOamso -->
<!ENTITY and      CDATA "&#8743;" -- ���������� �, U+2227 ISOtech -->
<!ENTITY or       CDATA "&#8744;" -- ���������� ���, U+2228 ISOtech -->
<!ENTITY cap      CDATA "&#8745;" -- ����������� = cap, U+2229 ISOtech -->
<!ENTITY cup      CDATA "&#8746;" -- union = cup, U+222A ISOtech -->
<!ENTITY int      CDATA "&#8747;" -- ��������, U+222B ISOtech -->
<!ENTITY there4   CDATA "&#8756;" -- �������������, U+2234 ISOtech -->
<!ENTITY sim      CDATA "&#8764;" -- �������� ������ = varies with = �������, U+223C ISOtech -->
<!-- �������� tilde ��� �� �� ��, ��� ������ tilde, U+007E,
     ���� ����� ���� ����������� ���� ���� ��� ����������� ����� ��������  -->
<!ENTITY cong     CDATA "&#8773;" -- �������������� �����, U+2245 ISOtech -->
<!ENTITY asymp    CDATA "&#8776;" --����� �����, U+2248 ISOamsr -->
<!ENTITY ne       CDATA "&#8800;" -- �� �����, U+2260 ISOtech -->
<!ENTITY equiv    CDATA "&#8801;" -- ���������, U+2261 ISOtech -->
<!ENTITY le       CDATA "&#8804;" -- ������ ��� �����, U+2264 ISOtech -->
<!ENTITY ge       CDATA "&#8805;" -- ������ ��� �����, U+2265 ISOtech -->
<!ENTITY sub      CDATA "&#8834;" -- subset of, U+2282 ISOtech -->
<!ENTITY sup      CDATA "&#8835;" -- superset of, U+2283 ISOtech -->
<!--������, ��� nsup, 'not a superset of, U+2283' �� ����������� ������������ ������
     Symbol � �� ������ � ����. ����� �� ���, ��� ���������? � ISOamsn  --> 
<!ENTITY nsub     CDATA "&#8836;" -- �� subset of, U+2284 ISOamsn -->
<!ENTITY sube     CDATA "&#8838;" -- subset of ��� ������������, U+2286 ISOtech -->
<!ENTITY supe     CDATA "&#8839;" -- superset of ��� ������������, U+2287 ISOtech -->
<!ENTITY oplus    CDATA "&#8853;" -- circled plus = direct sum, U+2295 ISOamsb -->
<!ENTITY otimes   CDATA "&#8855;" -- circled times = vector product, U+2297 ISOamsb -->
<!ENTITY perp     CDATA "&#8869;" -- up tack = ������������ � = perpendicular, U+22A5 ISOtech -->
<!ENTITY sdot     CDATA "&#8901;" -- �������� �����, U+22C5 ISOamsb -->
<!-- �������� ����� ��� �� ��� �� ����� ������, ��� U+00B7 ������� ����� -->

<!-- ��������� ����������� -->
<!ENTITY lceil    CDATA "&#8968;" -- left ceiling = apl upstile, U+2308 ISOamsc  -->
<!ENTITY rceil    CDATA "&#8969;" -- right ceiling, U+2309 ISOamsc  -->
<!ENTITY lfloor   CDATA "&#8970;" -- left floor = apl downstile, U+230A ISOamsc  -->
<!ENTITY rfloor   CDATA "&#8971;" -- right floor, U+230B ISOamsc  -->
<!ENTITY lang     CDATA "&#9001;" -- ������� ������ ����� = bra, U+2329 ISOtech -->
<!-- lang ��� �� ��� �� ����� ������, ��� U+003C '������ ���' 
     ��� U+2039 '��������� ������� ������ �����' -->
<!ENTITY rang     CDATA "&#9002;" -- ������� ������ ������ = ket, U+232A ISOtech -->
<!-- rang ��� �� ��� �� ����� ������, ��� U+003E '������ ���' 
     ��� U+203A '��������� ������� ������ ������' -->

<!-- �������������� ������ -->
<!ENTITY loz      CDATA "&#9674;" -- ����, U+25CA ISOpub -->

<!-- ��������� ������� -->
<!ENTITY spades   CDATA "&#9824;" -- ������ "����", U+2660 ISOpub -->
<!-- ������ �������� ����� "�������������" -->
<!ENTITY clubs    CDATA "&#9827;" -- ������ "������" = shamrock, U+2663 ISOpub -->
<!ENTITY hearts   CDATA "&#9829;" -- ������ "�����" = valentine, U+2665 ISOpub -->
<!ENTITY diams    CDATA "&#9830;" -- ������ "�����", U+2666 ISOpub -->

<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<meta http-equiv="Content-Language" content="ru"><title></title></head>
