<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Version Information</title>
<meta name="generator" content="DocBook XSL Stylesheets V1.68.1">
<link rel="start" href="index.html" title="Cairo: A Vector Graphics Library">
<link rel="up" href="Support.html" title="Utilities">
<link rel="prev" href="cairo-Error-handling.html" title="Error handling">
<link rel="next" href="cairo-Types.html" title="Types">
<meta name="generator" content="GTK-Doc V1.4 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
<link rel="part" href="pt01.html" title="Part&#160;I.&#160;Tutorial">
<link rel="part" href="pt02.html" title="Part&#160;II.&#160;Reference">
<link rel="chapter" href="Drawing.html" title="Drawing">
<link rel="chapter" href="Fonts.html" title="Fonts">
<link rel="chapter" href="Surfaces.html" title="Surfaces">
<link rel="chapter" href="Support.html" title="Utilities">
<link rel="index" href="ix01.html" title="Index">
<link rel="appendix" href="language-bindings.html" title="Appendix&#160;A.&#160;Creating a language binding for cairo">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" width="100%" summary="Navigation header" cellpadding="2" cellspacing="2"><tr valign="middle">
<td><a accesskey="p" href="cairo-Error-handling.html"><img src="left.png" width="24" height="24" border="0" alt="Prev"></a></td>
<td><a accesskey="u" href="Support.html"><img src="up.png" width="24" height="24" border="0" alt="Up"></a></td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="24" height="24" border="0" alt="Home"></a></td>
<th width="100%" align="center">Cairo: A Vector Graphics Library</th>
<td><a accesskey="n" href="cairo-Types.html"><img src="right.png" width="24" height="24" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry" lang="en">
<a name="cairo-Version-Information"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle">Version Information</span></h2>
<p>Version Information &#8212; Compile-time and run-time version checks.</p>
</td>
<td valign="top" align="right"></td>
</tr></table></div>
<div class="refsynopsisdiv">
<h2>Synopsis</h2>
<pre class="synopsis">



#define     <a href="cairo-Version-Information.html#CAIRO-VERSION:CAPS">CAIRO_VERSION</a>
#define     <a href="cairo-Version-Information.html#CAIRO-VERSION-ENCODE:CAPS">CAIRO_VERSION_ENCODE</a>            (major, minor, micro)
int         <a href="cairo-Version-Information.html#cairo-version">cairo_version</a>                   (void);
const char* <a href="cairo-Version-Information.html#cairo-version-string">cairo_version_string</a>            (void);
</pre>
</div>
<div class="refsect1" lang="en">
<a name="id2602661"></a><h2>Description</h2>
<p>
Cairo has a three-part version number scheme. In this scheme, we use
even vs. odd numbers to distinguish fixed points in the software
vs. in-progress development, (such as from CVS instead of a tar file,
or as a "snapshot" tar file as opposed to a "release" tar file).
</p>
<p>
</p>
<div class="informalexample"><pre class="programlisting">
 _____ Major. Always 1, until we invent a new scheme.
/  ___ Minor. Even/Odd = Release/Snapshot (tar files) or Branch/Head (CVS)
| /  _ Micro. Even/Odd = Tar-file/CVS
| | /
1.0.0
</pre></div>
<p>
</p>
<p>
Here are a few examples of versions that one might see.
</p>
<div class="informalexample"><pre class="programlisting">
Releases
--------
1.0.0 - A major release
1.0.2 - A subsequent maintenance release
1.2.0 - Another major release

Snapshots
---------
1.1.2 - A snapshot (working toward the 1.2.0 release)

In-progress development (eg. from CVS)
--------------------------------------
1.0.1 - Development on a maintenance branch (toward 1.0.2 release)
1.1.1 - Development on head (toward 1.1.2 snapshot and 1.2.0 release)
</pre></div>
<p>
</p>
<div class="refsect2" lang="en">
<a name="id2669917"></a><h3>Compatibility</h3>
<p>
The API/ABI compatibility guarantees for various versions are as
follows. First, let's assume some cairo-using application code that is
successfully using the API/ABI "from" one version of cairo. Then let's
ask the question whether this same code can be moved "to" the API/ABI
of another version of cairo.
</p>
<p>
Moving from a release to any later version (release, snapshot,
development) is always guaranteed to provide compatibility.
</p>
<p>
Moving from a snapshot to any later version is not guaranteed to
provide compatibility, since snapshots may introduce new API that ends
up being removed before the next release.
</p>
<p>
Moving from an in-development version (odd micro component) to any
later version is not guaranteed to provide compatibility. In fact,
there's not even a guarantee that the code will even continue to work
with the same in-development version number. This is because these
numbers don't correspond to any fixed state of the software, but
rather the many states between snapshots and releases.
</p>
</div>
<hr>
<div class="refsect2" lang="en">
<a name="id2669955"></a><h3>Examining the version</h3>
<p>
Cairo provides the ability to examine the version at either
compile-time or run-time and in both a human-readable form as well as
an encoded form suitable for direct comparison. Cairo also provides a
macro (CAIRO_VERSION_ENCODE) to perform the encoding.
</p>
<p>
</p>
<div class="informalexample"><pre class="programlisting">
Compile-time
------------
<code class="literal">CAIRO_VERSION_STRING</code>	Human-readable
<code class="literal">CAIRO_VERSION</code>		Encoded, suitable for comparison

Run-time
--------
<a href="cairo-Version-Information.html#cairo-version-string"><code class="function">cairo_version_string()</code></a>	Human-readable
<a href="cairo-Version-Information.html#cairo-version"><code class="function">cairo_version()</code></a>		Encoded, suitable for comparison
</pre></div>
<p>
</p>
<p>
For example, checking that the cairo version is greater than or equal
to 1.0.0 could be achieved at compile-time or run-time as follows:

</p>
<div class="informalexample"><pre class="programlisting">
#<span class="type">if</span> <code class="literal">CAIRO_VERSION</code> &gt;= <code class="literal">CAIRO_VERSION_ENCODE</code>(1, 0, 0)
printf ("Compiling with suitable cairo version: %<code class="literal">s</code>\n", CAIRO_VERSION_STRING);
#<span class="type">endif</span>

if (<a href="cairo-Version-Information.html#cairo-version"><code class="function">cairo_version()</code></a> &gt;= <code class="literal">CAIRO_VERSION_ENCODE</code>(1, 0, 0))
    printf ("Running with suitable cairo version: %<code class="literal">s</code>\n", <a href="cairo-Version-Information.html#cairo-version-string"><code class="function">cairo_version_string()</code></a>);
</pre></div>
<p>
</p>
</div>
</div>
<div class="refsect1" lang="en">
<a name="id2661900"></a><h2>Details</h2>
<div class="refsect2" lang="en">
<a name="id2661907"></a><h3>
<a name="CAIRO-VERSION:CAPS"></a>CAIRO_VERSION</h3>
<a class="indexterm" name="id2661917"></a><pre class="programlisting">#define     CAIRO_VERSION</pre>
<p>

</p>
</div>
<hr>
<div class="refsect2" lang="en">
<a name="id2661932"></a><h3>
<a name="CAIRO-VERSION-ENCODE:CAPS"></a>CAIRO_VERSION_ENCODE()</h3>
<a class="indexterm" name="id2661941"></a><pre class="programlisting">#define     CAIRO_VERSION_ENCODE(major, minor, micro)</pre>
<p>

</p>
<div class="variablelist"><table border="0">
<col align="left" valign="top">
<tbody>
<tr>
<td>
<span class="term"><em class="parameter"><code>major</code></em>&#160;:</span></td>
<td>
</td>
</tr>
<tr>
<td>
<span class="term"><em class="parameter"><code>minor</code></em>&#160;:</span></td>
<td>
</td>
</tr>
<tr>
<td>
<span class="term"><em class="parameter"><code>micro</code></em>&#160;:</span></td>
<td>


</td>
</tr>
</tbody>
</table></div>
</div>
<hr>
<div class="refsect2" lang="en">
<a name="id2667879"></a><h3>
<a name="cairo-version"></a>cairo_version ()</h3>
<a class="indexterm" name="id2667889"></a><pre class="programlisting">int         cairo_version                   (void);</pre>
<p>
Returns the version of the cairo library encoded in a single
integer as per CAIRO_VERSION_ENCODE. The encoding ensures that
later versions compare greater than earlier versions.
</p>
<p>
A run-time comparison to check that cairo's version is greater than
or equal to version X.Y.Z could be performed as follows:
</p>
<p>
</p>
<div class="informalexample"><pre class="programlisting">
if (<a href="cairo-Version-Information.html#cairo-version"><code class="function">cairo_version()</code></a> &gt;= CAIRO_VERSION_ENCODE(X,Y,Z)) {...}
</pre></div>
<p>
</p>
<p>
See also <a href="cairo-Version-Information.html#cairo-version-string"><code class="function">cairo_version_string()</code></a> as well as the compile-time
equivalents <code class="literal">CAIRO_VERSION</code> and <code class="literal">CAIRO_VERSION_STRING</code>.</p>
<p>

</p>
<div class="variablelist"><table border="0">
<col align="left" valign="top">
<tbody><tr>
<td>
<span class="term"><span class="emphasis"><em>Returns</em></span>&#160;:</span></td>
<td> the encoded version.
</td>
</tr></tbody>
</table></div>
</div>
<hr>
<div class="refsect2" lang="en">
<a name="id2667984"></a><h3>
<a name="cairo-version-string"></a>cairo_version_string ()</h3>
<a class="indexterm" name="id2667993"></a><pre class="programlisting">const char* cairo_version_string            (void);</pre>
<p>
Returns the version of the cairo library as a human-readable string
of the form "X.Y.Z".
</p>
<p>
See also <a href="cairo-Version-Information.html#cairo-version"><code class="function">cairo_version()</code></a> as well as the compile-time equivalents
<code class="literal">CAIRO_VERSION_STRING</code> and <code class="literal">CAIRO_VERSION</code>.</p>
<p>

</p>
<div class="variablelist"><table border="0">
<col align="left" valign="top">
<tbody><tr>
<td>
<span class="term"><span class="emphasis"><em>Returns</em></span>&#160;:</span></td>
<td> a string containing the version.
</td>
</tr></tbody>
</table></div>
</div>
</div>
</div>
<table class="navigation" width="100%" summary="Navigation footer" cellpadding="2" cellspacing="0"><tr valign="middle">
<td align="left"><a accesskey="p" href="cairo-Error-handling.html"><b>&lt;&lt;&#160;Error handling</b></a></td>
<td align="right"><a accesskey="n" href="cairo-Types.html"><b>Types&#160;&gt;&gt;</b></a></td>
</tr></table>
</body>
</html>
