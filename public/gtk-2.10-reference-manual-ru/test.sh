#!/bin/bash

for file in `find *.html`
do
        err=`cat $file | aspell -d ru -a | grep '^&' | wc -l`
        test $err -gt 0 && echo "$file: $err"
done
